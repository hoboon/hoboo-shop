<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : ApiHandler
 * @Author          : mr.box
 * @Createtime      : 2022/3/9 12:52 AM
 * @Description     : ...
 */


namespace support\exception;


class ApiHandler extends \RuntimeException
{
    protected $error;
    
    public function __construct($error)
    {
        var_dump($error);
        $this->error = $error;
        $this->message = is_array($error) ? implode(PHP_EOL, $error) : $error;
    }
    
}
