<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : ThinkOrm
 * @Author          : mr.box
 * @Createtime      : 2022/2/28 4:43 PM
 * @Description     : ...
 */


namespace support\bootstrap;


use think\facade\Db;
use Webman\Bootstrap;
use Workerman\Timer;

class ThinkOrm implements Bootstrap
{
    public static function start($worker)
    {
        Db::setConfig(config('thinkorm'));
        
        if ($worker) {
            Timer::add(55, function () {
                $connection = config('thinkorm.connections', []);
                
                foreach ($connection as $key => $item) {
                    if ($item['type'] === 'mysql') {
                        Db::connect($key)->query('select 1');
                    }
                }
            });
        }
    }
}
