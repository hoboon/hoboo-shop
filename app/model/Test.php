<?php

namespace app\model;

// use support\Model;
use think\Model;

class Test extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    // protected $table = 'test';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    // protected $primaryKey = 'id';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;
    
    /**
     * 模型名（相当于不带数据表前后缀的表名，默认为当前模型类名）
     * @var string
     */
    protected $name = 'hb_test';
    
    /**
     * 数据表名（默认自动获取）
     * @var string
     */
    protected $table = 'hb_test';
    
    /**
     * 数据表后缀（默认为空）
     *
     * @var string
     */
    protected $suffix = '';
    
    /**
     * 主键名
     *
     * @var string
     */
    protected $pk = 'id';
    
    /**
     * 模型允许写入的字段列表（数组）
     *
     * @var array
     */
    protected $field = [];
    
    /**
     * 模型对应数据表字段及类型（数组）
     *
     * @var array
     */
    protected $schema = [];
    
    /**
     * 模型需要自动转换的字段及类型（数组）
     *
     * @var array
     */
    protected $type = [];
    
    /**
     * 模型需要隐藏的字段
     * @var string[]
     */
    protected $hidden = [
        'delete_time'
    ];
    
    // 搜索器
    public function searchNameAttr($query, string $valur)
    {
        $query->where('name',);
    }
    
    /**
     * @desc 列表获取
     * @param  array  $data
     * @return array|Test[]
     * @throws
     */
    public static function getList(array $data): array
    {
        $result['data'] = $data;
        
        // 搜索条件
        $map = [];

        if (!empty($data['keywords'])) {
            $map[] = ['name', 'like', "%" . $data['keywords'] . "%"];
        }
        
        // 数据统计
        $result['total_result'] = (new self())->where($map)->count();
        
        // 统计判断
        if (!$result['total_result']) {
            return $result;
        }
        
        // 真实查询
        $result['items'] = (new self())->where($map)->select();
        
        $result['_sql'] = (new self())->getLastSql();
        
        return $result;
    }
    
}
