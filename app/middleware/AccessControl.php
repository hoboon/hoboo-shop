<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : AccessControlTest
 * @Author          : mr.box
 * @Createtime      : 2022/3/7 9:10 AM
 * @Description     : ...
 */


namespace app\middleware;


use hoboo\exception\UnauthorizedHttpException;
use Tinywan\Jwt\Exception\JwtTokenException;
use Tinywan\Jwt\JwtToken;
use Webman\Http\Request;
use Webman\Http\Response;
use Webman\MiddlewareInterface;

class AccessControl implements MiddlewareInterface
{
    /**
     * @param  Request  $request
     * @param  callable  $handler
     * @return Response
     * @throws UnauthorizedHttpException
     */
    public function process(Request $request, callable $handler): Response
    {
        /**
         * 跨域处理
         */
        $response = $request->method() == 'OPTIONS' ? response('') : $handler($request);
        $response->withHeaders([
            'Access-Control-Allow-Origin'   => '*',
            // 'Access-Control-Allow-Origin'  => 'https://hb-tjd.demo.hobooa.com',
            'Access-Control-Allow-Methods'  => 'GET,POST,PUT,DELETE,OPTIONS',
            'Access-Control-Allow-Headers'  => 'Content-Type,Authorization,X-Requested-With,Accept,Origin',
            'Access-Control-Expose-Headers' => 'Token-Type,Expires-In,Access-Token,Refresh-Token'
        ]);
        
        /**
         * token 鉴权（debug 模式下不做鉴权）
         */
        if (!config('app.debug') && !in_array($request->path(), $this->getCommonPath())) {
            try {
                JwtToken::getCurrentId();
            } catch (JwtTokenException $e) {
                throw new UnauthorizedHttpException($e->getMessage());
            }
        }
        
        return $response;
    }
    
    /**
     * 公共接口路径（不需要验证Token）
     *
     * @return string[]
     */
    protected function getCommonPath(): array
    {
        return [
            '/api/auth/captcha',    // 获取验证码
            '/api/auth/login',      // 登陆验证
        ];
    }
}
