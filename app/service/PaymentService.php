<?php


namespace app\service;


use hoboo\service\HobooService;
use Yansongda\Pay\Pay;

class PaymentService extends HobooService
{
    //生成微信小程序支付预订单
    public function getMiniWexinPrePay($order,$userInfo,$wechatUser)
    {
        $config=config("payment");
        Pay::config($config);

        $order = [
            'out_trade_no' => $order["order_id"],
            'description' => "支付订单".$order['order_id'],
            'amount' => [
                'total' => $order["pay_price"]*100,//1分，需要传过来的订单金额*100
                'currency' => 'CNY',
            ],
            'payer' => [
                'openid' => $wechatUser["openid"],
            ]
        ];
//        dd($order);
        $result=Pay::wechat()->mini($order);
        return $result;
    }


}