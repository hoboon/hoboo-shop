<?php


namespace app\service;


use app\api\model\StoreCombination;
use app\api\model\StoreOrder;
use app\api\model\StorePink;
use app\api\model\User;
use hoboo\service\HobooService;
use Tinywan\ExceptionHandler\Exception\BadRequestHttpException;

class StorePinkService extends HobooService
{
    public function __construct()
    {
        $this->model=new StorePink();
    }


}