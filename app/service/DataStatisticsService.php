<?php


namespace app\service;


use app\api\model\StoreOrder;
use app\api\model\User;
use app\api\model\UserVisit;
use hoboo\service\HobooService;

class DataStatisticsService extends HobooService
{
    public function todayYesterdayOrderStatistic()
    {
        //统计今日与昨日订单数据
        //今日，昨日
        $yesterday=date("Y-m-d",strtotime("-1 day"));
        $today=date("Y-m-d",time());
        //统计昨日
        $yesterdayStart=$yesterday." 00:00:00";
        $yesterdayEnd=$yesterday." 23:59:59";
        $yesterdaySalesOrder=StoreOrder::where("paid",1)->whereBetween("create_time",[$yesterdayStart,$yesterdayEnd])->get();

        //统计今日
        $todayStart=$today." 00:00:00";
        $todayEnd=$today." 23:59:59";
        $todaySalesOrder=StoreOrder::where("paid",1)->whereBetween("create_time",[$todayStart,$todayEnd])->get();

        return [$yesterdaySalesOrder,$todaySalesOrder];

    }
    //本月数据格式化
    public function monthFormat()
    {
        //当月第一天
        $firstDate=date("Y-m-01",time());
        //当月最后一天
        $lastDate=date("Y-m-d",strtotime("$firstDate+ 1 month -1 day"));

        return [$firstDate,$lastDate];
    }
    //今日昨日数据格式化
    public function dayFormat()
    {
        //今日，昨日
        $yesterday=date("Y-m-d",strtotime("-1 day"));
        $today=date("Y-m-d",time());
        $yesterdayStart=$yesterday." 00:00:00";
        $yesterdayEnd=$yesterday." 23:59:59";
        $todayStart=$today." 00:00:00";
        $todayEnd=$today." 23:59:59";
        return [$yesterdayStart,$yesterdayEnd,$todayStart,$todayEnd];
    }
    //统计本月销售额
    public function monthSales()
    {
        [$firstDate,$lastDate]=$this->monthFormat();
        $firstDate=$firstDate." 00:00:00";
        $lastDate=$lastDate." 23:59:59";
        $monthSaledata=StoreOrder::where("paid",1)->whereBetween("create_time",[$firstDate,$lastDate])->get();
        $monthTotal=0.00;
        foreach ($monthSaledata as $k=>$v){
            $monthTotal+=$v["pay_price"];
        }
        return $monthTotal;
    }
    //顶部销售额数据统计
    public function topCardSalesStatistic()
    {
        $data[0]=[
            "title"=>"销售额",
            "period"=>"今日"
        ];
        [$yesterdaySalesOrder,$todaySalesOrder]=$this->todayYesterdayOrderStatistic();
        //今日销售额
        $todayTotal=0.00;
        foreach ($todaySalesOrder as $k=>$v){
            $todayTotal+=$v["pay_price"];
        }
        $yesterdayTotal=0.00;
        foreach ($yesterdaySalesOrder as $k=>$v){
            $yesterdayTotal+=$v["pay_price"];
        }

        $data[0]["total"]=$todayTotal;

        $data[0]["subs"][0]["title"]="昨日";
        $data[0]["subs"][0]["value"]=$yesterdayTotal;
        $data[0]["subs"][1]["title"]="日环比";
        $minusValue=$todayTotal-$yesterdayTotal;
        if($minusValue>=0){
            $data[0]["subs"][1]["value"]=($minusValue*100)."%";
            $data[0]["subs"][1]["upAndDown"]="up";
        }else{
            $data[0]["subs"][1]["value"]="-".($minusValue*100)."%";
            $data[0]["subs"][1]["upAndDown"]="down";
        }
        $monthTotal=$this->monthSales();
        $data[0]["bottom"]["title"]="本月销售额";
        $data[0]["bottom"]["value"]=$monthTotal;

        return $data;
    }

    //获取用户访问量
    public function userVisitData()
    {
        $data=[
            "title"=>"用户访问量",
            "period"=>"今日",
        ];
        [$yesterdayStart,$yesterdayEnd,$todayStart,$todayEnd]=$this->dayFormat();

        $todayUserVisit=UserVisit::whereBetween("create_time",[$todayStart,$todayEnd])->count();
        $data["total"]=$todayUserVisit;
        $data["subs"][0]["title"]="昨日";
        $yesterdayVisit=UserVisit::whereBetween("create_time",[$yesterdayStart,$yesterdayEnd])->count();
        $data["subs"][0]["value"]=$yesterdayVisit;

        //日环比
        $data["subs"][1]["title"]="日环比";
        $minusValue=$todayUserVisit-$yesterdayVisit;
        if($minusValue>=0){
            $data["subs"][1]["value"]=($minusValue*100)."%";
            $data["subs"][1]["upAndDown"]="up";
        }else{
            $data["subs"][1]["value"]=($minusValue*100)."%";
            $data["subs"][1]["upAndDown"]="down";
        }
        //统计本月访问量
        [$firstDate,$lastDate]=$this->monthFormat();
        $monthUserVisit=UserVisit::whereBetween("create_time",[$firstDate,$lastDate])->count();

        $data["bottom"]["title"]="本月访问量";
        $data["bottom"]["value"]=$monthUserVisit."Pv";

        return $data;
    }
    //获取订单量
    public function orderCountStatistic()
    {
        $data=[
            "title"=>"订单量",
            "period"=>"今日",
        ];
        [$yesterdayStart,$yesterdayEnd,$todayStart,$todayEnd]=$this->dayFormat();
        $todayOrderCount=StoreOrder::whereBetween("create_time",[$todayStart,$todayEnd])->count();
        $data["total"]=$todayOrderCount;
        $yesterdayOrderCount=StoreOrder::whereBetween("create_time",[$yesterdayStart,$yesterdayEnd])->count();
        $data["subs"][0]["title"]="昨日";
        $data["subs"][0]["value"]=$yesterdayOrderCount;
        //日环比
        $data["subs"][1]["title"]="日环比";
        $minusValue=$todayOrderCount-$yesterdayOrderCount;
        if($minusValue>=0){
            $data["subs"][1]["value"]=($minusValue*100)."%";
            $data["subs"][1]["upAndDown"]="up";
        }else{
            $data["subs"][1]["value"]=($minusValue*100)."%";
            $data["subs"][1]["upAndDown"]="down";
        }
        //统计本月订单量
        [$firstDate,$lastDate]=$this->monthFormat();
        $monthStoreOrderCount=StoreOrder::whereBetween("create_time",[$firstDate,$lastDate])->count();
        $data["bottom"]["title"]="本月订单量";
        $data["bottom"]["value"]=$monthStoreOrderCount."单";

        return $data;

    }
    public function newUserCount()
    {
        $data=[
            "title"=>"新增用户",
            "period"=>"今日",
        ];
        [$yesterdayStart,$yesterdayEnd,$todayStart,$todayEnd]=$this->dayFormat();
        $todayUser=User::whereBetween("create_time",[$todayStart,$todayEnd])->count();
        $data["total"]=$todayUser;
        $data["subs"][0]["title"]="昨日";
        //昨日新增用户统计
        $yesterdayUser=User::whereBetween("create_time",[$yesterdayStart,$yesterdayEnd])->count();
        $data["subs"][0]["value"]=$yesterdayUser;

        //日环比
        $data["subs"][1]["title"]="日环比";
        $minusValue=$todayUser-$yesterdayUser;
        if($minusValue>=0){
            $data["subs"][1]["value"]=($minusValue*100)."%";
            $data["subs"][1]["upAndDown"]="up";
        }else{
            $data["subs"][1]["value"]=($minusValue*100)."%";
            $data["subs"][1]["upAndDown"]="down";
        }

        //统计本月订单量
        [$firstDate,$lastDate]=$this->monthFormat();
        $monthUserCount=User::whereBetween("create_time",[$firstDate,$lastDate])->count();
        $data["bottom"]["title"]="本月新增用户";
        $data["bottom"]["value"]=$monthUserCount;

        return $data;
    }

    //统计待付款订单
    public function unPaidOrderCount()
    {
        $data=[
            "logo"=>"https://shop.hb.hobooa.com/images/dfk.png",
            "title"=>"待付款订单",
        ];
        $unPaidOrderCount=StoreOrder::where("paid",0)->count();
        $data["value"]=$unPaidOrderCount;
        return $data;
    }
    //统计待发货订单
    public function unDeliverOrderCount()
    {
        $data=[
            "logo"=>"https://shop.hb.hobooa.com/images/dfh.png",
            "title"=>"待发货订单",
        ];
        $unDeliverOrderCount=StoreOrder::where(["paid"=>1,"state"=>0])->count();
        $data["value"]=$unDeliverOrderCount;
        return $data;
    }
    //统计待收货
    public function unReceiveOrderCount()
    {
        $data=[
            "logo"=>"https://shop.hb.hobooa.com/images/dsh.png",
            "title"=>"待收货订单",
        ];
        $unReceiveOrderCount=StoreOrder::where(["paid"=>1,"state"=>1])->count();
        $data["value"]=$unReceiveOrderCount;
        return $data;
    }
    //统计售后订单
    public function afterSaleCount()
    {
        $data=[
            "logo"=>"https://shop.hb.hobooa.com/images/sh.png",
            "title"=>"售后订单",
        ];
        $afterSaleCount=StoreOrder::where([
            ["state","<",0]
        ])->count();
        $data["value"]=$afterSaleCount;
        return $data;
    }

}