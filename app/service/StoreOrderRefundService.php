<?php


namespace app\service;


//订单退款处理服务类
use app\api\model\StoreOrder;
use app\api\model\StoreOrderStatus;
use hoboo\service\HobooService;
use support\Db;
use Tinywan\ExceptionHandler\Exception\BadRequestHttpException;

class StoreOrderRefundService extends HobooService
{
    public function __construct()
    {
        $this->model=new StoreOrder();
    }

    /**
     * 订单申请退款
     * @param $order
     * @param string $refundReasonWap
     * @param string $refundReasonWapExplain
     * @param array $refundReasonWapImg
     * @param int $refundType
     * @throws BadRequestHttpException
     */
    public function orderApplyRefund($order,$refundReasonWap='',$refundReasonWapExplain='',$refundReasonWapImg=[],$refundType=0)
    {
        if (!$order) {
            throw new BadRequestHttpException('支付订单不存在!');
        }
        if ($order['refund_status'] == 2) {
            throw new BadRequestHttpException('订单已退款!');
        }
        if ($order['refund_status'] == 1) {
            throw new BadRequestHttpException('正在申请退款中!');
        }

        $old_status=0;
        $old_order_id=$order["id"];
        //todo:查询是否拆分子订单 存在子订单要退款主订单 拆分剩余商品申请退款


        //退款
        $data=[
            'refund_status' => 1,
            'refund_reason_time' => time(),
            'refund_reason_wap' => $refundReasonWap,
            'refund_reason_wap_explain' => $refundReasonWapExplain,
            'refund_reason_wap_img' => json_encode($refundReasonWapImg),
            'refund_type' => $refundType,
            "id"=>$order["id"],
            "state"=>-1
        ];
        try {
            Db::beginTransaction();
            if($old_status){
                //改变原来的订单状态
                $result=$this->model::setItem([
                    "id"=>$old_order_id,
                    "status"=>$old_status
                ]);
            }
            //更新订单状态,记录入order_status表
//            $statusResult=StoreOrderStatus::insert([
//                "oid"=>$order["id"],
//                "change_type"=>"apply_refund",
//                'change_message' => '用户申请退款，原因：' . $refundReasonWap,
//            ]);
            //更新订单的退款状态
            $orderResult=$this->model::setItem($data);

//            $orderData=$orderResult["data"];


            Db::commit();
            return $orderResult;
        }catch (BadRequestHttpException $e){
            Db::rollBack();
            throw $e;
        }
    }

}