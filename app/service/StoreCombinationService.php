<?php


namespace app\service;


use app\api\model\StorePink;
use app\api\model\User;
use hoboo\service\HobooService;
use app\api\model\StoreCombination as StoreCombinationModel;
use Tinywan\ExceptionHandler\Exception\BadRequestHttpException;


class StoreCombinationService extends HobooService
{

    public function __construct()
    {
        $this->model=new StoreCombinationModel();
    }
    public function getCombinationList($data)
    {
        $result=$this->model::getList($data);
        $list=$result["data"];
        foreach ($list as &$item){
            $item['price'] = floatval($item['price']);
            $item["getProduct"]['price'] = floatval($item["getProduct"]['price']);
        }
        $result["data"]=$list;
        return $result;
    }

    /**
     * 得到拼团商品详情
     * @param $data
     * @return array
     * @throws BadRequestHttpException
     */
    public function combinationDetail($data)
    {

        $combinationResult=$this->model::getItem($data);
        $combinationData=$combinationResult["data"];

        if(empty($combinationData)){
            throw new BadRequestHttpException("该商品不存在");
        }
        //处理图片链接

        //todo:获取拼团进行中列表

        //todo:获取活动购买数量

        //获取浏览记录

        return $combinationResult;
    }

    /**
     * 获取用户订单拼团详情
     * @param $params
     */
    public function getPinkInfo($params)
    {
        $pinkService=new StorePinkService();
        $is_ok = 0;//判断拼团是否完成
        $userBool = 0;//判断当前用户是否在团内  0未在 1在
        $pinkBool = 0;//判断拼团是否成功  0未在 1在

        $userResult=User::getItem(["id"=>$params["uid"]]);
        $userData=$userResult["data"];
        if(empty($params["pink_id"])){
            throw new BadRequestHttpException("参数错误");
        }
        $pinkResult=StorePink::getItem(["id"=>$params["pink_id"]]);
        $pink=$pinkResult["data"];
        if(empty($pink)){
            throw new BadRequestHttpException("参数错误");
        }
        if(isset($pink['is_refund']) && $pink['is_refund']){
            if ($pink['is_refund'] != $pink['id']) {
                $id = $pink['is_refund'];
                return $this->getPinkInfo($params);
            } else {
                throw new BadRequestHttpException('订单已退款');
            }
        }
        list($pinkAll, $pinkT, $count, $idAll, $uidAll)=$pinkService->getPinkMemberAndPinkK($pink);
        if ($pinkT['status'] == 2) {
            $pinkBool = 1;
            $is_ok = 1;
        } else if ($pinkT['status'] == 3) {
            $pinkBool = -1;
            $is_ok = 0;
        } else {
            if ($count < 1) {//组团完成
                $is_ok = 1;
                $pinkBool = $pinkService->pinkComplete($uidAll, $idAll, $userData['id'], $pinkT);
            } else {
                $pinkBool = $pinkService->pinkFail($pinkAll, $pinkT, $pinkBool);
            }
        }

        if (!empty($pinkAll)) {
            foreach ($pinkAll as $v) {
                if ($v['uid'] == $userData['id']){
                    $userBool = 1;
                }
            }
        }
        if ($pinkT['uid'] == $userData['id']){
            $userBool = 1;
        }
        $combinationOne = $this->getCombinationOne($pink['cid']);
        if(empty($combinationOne)){
            throw new BadRequestHttpException("拼团不存在或已下架,请手动申请退款!");

        }
        $data["userInfo"]["uid"]=$userData["id"];
        $data["userInfo"]["nickname"]=$userData["nickname"];
        $data["userInfo"]["avatar"]=$userData["avatar"];
        $data["is_ok"]=$is_ok;
        $data['userBool'] = $userBool;
        $data['pinkBool'] = $pinkBool;
        $data['store_combination'] = $combinationOne;
        $data['pinkT'] = $pinkT;
        $data['pinkAll'] = $pinkAll;
        $data['count'] = $count <= 0 ? 0 : $count;
        $data['store_combination_host'] ="";//后期待完善getCombinationHost方法
        $data['current_pink_order'] ="";//后期待完善$pinkService->getCurrentPink方法

        //todo:根据拼团商品id获取商品模型、商品规格与商品属性


        //使用之前的查询值的data进行值绑定
        $pinkResult["data"]=$data;
        return $pinkResult;

    }

    /**
     * @param $combination_id
     * 获取一条拼团数据
     */
    public function getCombinationOne($combination_id)
    {
        $combinationResult=$this->model::getItem([
            "is_show"=>1,
            "id"=>$combination_id,
        ]);
        $combinationData=$combinationResult["data"];
        return $combinationData;
    }





}