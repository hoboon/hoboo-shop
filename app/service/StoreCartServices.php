<?php


namespace app\service;


use app\api\model\StoreCart;
use hoboo\service\HobooService;
use support\Model;

class StoreCartServices extends HobooService
{
    public function __construct()
    {
        $this->model=new StoreCart();
    }

    public function getUserProductCartList($uid, $cartIds,$new,$product_id, $addr = [])
    {
        if($new){
            //立即购买的话，构建一个购物车数组
            $cartInfo[]=[
                "id"=>time(),
                "uid"=>$uid,
                "type"=>"product",
                "product_id"=>$product_id,
                "cart_num"=>1,
                "is_new"=>1,
                "state"=>1
            ];
        }else{
            $cartIds=explode(",",rtrim($cartIds,","));
            $where=[
                "id"=>$cartIds
            ];
            $cartResult=$this->model::getList($where);
            $cartInfo=$cartResult["data"];
        }
        return $cartInfo;
    }

}