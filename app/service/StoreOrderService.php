<?php


namespace app\service;


use app\api\model\StoreOrder;
use app\api\model\StoreOrderCartInfo;
use hoboo\service\HobooService;

class StoreOrderService extends HobooService
{
    public function __construct()
    {
        $this->model=new StoreOrder();
    }
    public function tidyOrder($orderInfo)
    {
        $cartInfoResult=StoreOrderCartInfo::getList(["oid"=>$orderInfo["id"]]);
        $cartInfo=$cartInfoResult["data"];
        $info=[];
        foreach ($cartInfo as $k=>$v){
            $cart=json_decode($v["cart_info"],true);
            array_push($info,$cart);
        }
        $orderInfo["cartInfo"]=$info;
        return $orderInfo;
    }
    

}