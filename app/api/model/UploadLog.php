<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : UploadLog
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-07-11 05:05:27 PM
 * @Description     : ...
 */


namespace app\api\model;

use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use hoboo\model\HobooModel;


/**
 * @method withSearch(string[] $array, array $data)
 * @method where(array $array)
 * @method static create(array|string[] $_data)
 */
class UploadLog extends HobooModel
{
    
    /**
     * 与模型关联的数据表.
     *
     * @var string
     */
    protected $table = 'upload_log';
    
    /**
     * 允许批量赋值的字段 白名单设置
     *
     * @var string[]
     */
    protected $fillable = [];
    
    /**
     * 不允许批量赋值的字段 黑名单设置
     *
     * @var string[]
     */
    protected $guarded = [];
    
    /**
     * 数组中的属性会被隐藏。
     *
     * @var array
     */
    protected $hidden = ['delete_time'];
    
    /**
     * 类型转换
     *
     * @var array
     */
    protected $casts = [
        'create_time' => 'datetime:Y/m/d H:i:s',
        'update_time' => 'datetime:Y/m/d H:i:s',
    ];
    
    /**
     * 获取数据列表
     *
     * @param  array  $data
     * @return array
     * @throws
     */
    public static function getList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['state', 'date', 'keywords'];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        $data['total'] = $model->withSearch($search, $data)->count();
        
        /**
         * 分页参数预处理
         */
        $result = $model->handlePage($data);
        
        if (!$data['total']) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 搜索器追加分页请求参数
         */
        $search[] = 'page';
        $data = array_merge($data, $result);
        
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->orderBy('id', 'desc')->get();
        
        return $result;
    }
    
    /**
     * 获取一条数据记录
     *
     * @param  array  $data
     * @return array
     */
    public static function getItem(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['uuid', 'state', 'date', 'keywords'];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        if (!$model->withSearch($search, $data)->count()) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->sole();
        
        return $result;
    }
    
    /**
     * 创建或更新一条数据
     *
     * @param  array  $data
     * @return array
     */
    public static function setItem(array $data): array
    {
        unset($data['create_time'], $data['update_time']);
        
        /**
         * 初始化模型
         */
        $model = new self();
        if (isset($data['uuid']) && !$model->withSearch(['uuid'], $data)->count()) {
            $result['data'] = self::create($data);
            $result['message'] = '操作成功';
        } else {
            $result = [];
        }
        
        return $result;
    }
}
