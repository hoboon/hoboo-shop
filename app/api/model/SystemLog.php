<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : SystemLog
 * @Author          : mr.box
 * @Createtime      : 2022/3/3 5:12 PM
 * @Description     : ...
 */

namespace app\api\model;

use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use hoboo\model\HobooModel;
use Illuminate\Database\Eloquent\Builder;


/**
 * @method withSearch(string[] $array, array $data)
 * @method where(array $array)
 * @method static create(array|string[] $_data)
 */
class SystemLog extends HobooModel
{
    
    /**
     * 与模型关联的数据表.
     *
     * @var string
     */
    protected $table = 'system_log';
    
    /**
     * 允许批量赋值的字段 白名单设置
     *
     * @var string[]
     */
    protected $fillable = [];
    
    /**
     * 不允许批量赋值的字段 黑名单设置
     *
     * @var string[]
     */
    protected $guarded = [];
    
    /**
     * type 局部作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeOfType(Builder $query, $data): Builder
    {
        if (isset($data['type'])) {
            $query->where('type', 'like', '%'.$data['type'].'%');
        }
        
        return $query;
    }
    
    /**
     * 获取数据列表
     * @param  array  $data
     * @return array
     * @throws
     */
    public static function getList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['state', 'type', 'keywords'];
        $data['keywordsBy'] = $data['keywordsBy'] ?? 'admin_name';
        
        /**
         * 初始化模型
         */
        $model = new self();
    
        /**
         * 统计查询
         */
        $data['total'] = $model->withSearch($search, $data)->count();
    
        /**
         * 分页参数预处理
         */
        $result = $model->handlePage($data);
    
        if (!$data['total']) {
            $result['data'] = [];
            return $result;
        }
    
        /**
         * 搜索器追加分页请求参数
         */
        $search[] = 'page';
        $data = array_merge($data, $result);
    
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->orderBy('id', 'desc')->get();
    
        return $result;
    }
    
    /**
     * 创建或更新一条数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     */
    public static function setItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        unset($data['create_time'], $data['update_time']);
        
        if (isset($data['id'])) {
            $_data = $model->withSearch(['id'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } elseif (isset($data['uuid'])) {
            $_data = $model->withSearch(['uuid'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } else {
            $_data = array_merge($data, ['uuid' => uniqid()]);
            $result['data'] = self::create($_data);
            $result['message'] = '操作成功';
        }
        
        return $result;
    }
    
    /**
     * 提交数据更新
     *
     * @param $data
     * @param  array  $updateData
     * @return array
     * @throws BadRequestHttpException
     */
    private function handleUpdate($data, array $updateData): array
    {
        if (is_null($data)) {
            throw new BadRequestHttpException('参数提交错误');
        } else {
            $is_update = self::where(['id' => $data['id']])->update($updateData);
            $result['message'] = $is_update ? '修改成功' : '修改失败';
            $result['data'] = array_merge(
                $data->toArray(),
                $updateData,
                ['is_update' => $is_update]
            );
        }
        
        return $result;
    }
    
    /**
     * 删除一个系统日志
     * @param  array|string  $data
     * @return array
     */
    public static function delItem(array|string $data): array
    {
        if (!empty($data['id'])) {
            $data = !is_array($data['id']) ? explode(',', $data['id']) : $data['id'];
            self::destroy($data);
        }
        
        return self::getList($data);
    }
}
