<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : PmPublic
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-04-28 03:32:08 PM
 * @Description     : ...
 */


namespace app\api\model;

use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use hoboo\model\HobooModel;
use Illuminate\Database\Eloquent\Builder;


/**
 * @method withSearch(string[] $array, array $data)
 * @method where(array $array)
 * @method static create(array|string[] $_data)
 */
class PmPublic extends HobooModel
{
    
    /**
     * 与模型关联的数据表.
     *
     * @var string
     */
    protected $table = 'pm_public';
    
    /**
     * 允许批量赋值的字段 白名单设置
     *
     * @var string[]
     */
    protected $fillable = [];
    
    /**
     * 不允许批量赋值的字段 黑名单设置
     *
     * @var string[]
     */
    protected $guarded = [];
    
    /**
     * 数组中的属性会被隐藏。
     *
     * @var array
     */
    protected $hidden = ['delete_time'];
    
    /**
     * 类型转换
     *
     * @var array
     */
    protected $casts = [
        'follow_time'   => 'datetime:Y/m/d H:i:s',
        'delivery_time' => 'datetime:Y/m/d H:i:s',
        'start_time'    => 'datetime:Y/m/d H:i:s',
        'create_time'   => 'datetime:Y/m/d H:i:s',
        'update_time'   => 'datetime:Y/m/d H:i:s',
    ];
    
    /**
     * organization 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeOrganization(Builder $query, $data): Builder
    {
        if (isset($data['organization_uuid'])) {
            $query->where('organization_uuid', $data['organization_uuid']);
        }
        
        return $query;
    }
    
    /**
     * category 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeCategory(Builder $query, $data): Builder
    {
        if (isset($data['category_uuid'])) {
            $query->where('category_uuid', $data['category_uuid']);
        }
        
        return $query;
    }
    
    /**
     * status 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeStatus(Builder $query, $data): Builder
    {
        if (isset($data['status_uuid'])) {
            $query->where('status_uuid', $data['status_uuid']);
        }
        
        return $query;
    }
    
    /**
     * customer 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeCustomer(Builder $query, $data): Builder
    {
        if (isset($data['customer_uuid'])) {
            $query->where('customer_uuid', $data['customer_uuid']);
        }
        
        return $query;
    }
    
    /**
     * contact 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeContact(Builder $query, $data): Builder
    {
        if (isset($data['contact_uuid'])) {
            $query->where('contact_uuid', $data['contact_uuid']);
        }
        
        return $query;
    }
    
    /**
     * operator 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeOperator(Builder $query, $data): Builder
    {
        if (isset($data['operator_uuid'])) {
            $query->where('operator_uuid', $data['operator_uuid']);
        }
        
        return $query;
    }
    
    /**
     * priority 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopePriority(Builder $query, $data): Builder
    {
        if (isset($data['priority'])) {
            $query->where('priority', $data['priority']);
        }
        
        return $query;
    }
    
    /**
     * shared 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeShared(Builder $query, $data): Builder
    {
        if (isset($data['is_shared'])) {
            $query->where('is_shared', $data['is_shared']);
        }
        
        return $query;
    }
    
    /**
     * starred 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeStarred(Builder $query, $data): Builder
    {
        if (isset($data['is_starred'])) {
            $query->where('is_starred', $data['is_starred']);
        }
        
        return $query;
    }
    
    /**
     * 获取数据列表
     *
     * @param  array  $data
     * @return array
     * @throws
     */
    public static function getList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = [
            'state',
            'date',
            'keywords',
            
            'organization',
            'category',
            'status',
            'customer',
            'contact',
            'operator',
            'priority',
            'shared',
            'starred',
        ];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        $data['total'] = $model->withSearch($search, $data)->count();
        
        /**
         * 分页参数预处理
         */
        $result = $model->handlePage($data);
        
        if (!$data['total']) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 搜索器追加分页请求参数
         */
        $search[] = 'page';
        $data = array_merge($data, $result);
        
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->orderBy('id', 'desc')->get();
        
        return $result;
    }
    
    /**
     * 获取一条数据记录
     *
     * @param  array  $data
     * @return array
     */
    public static function getItem(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['uuid', 'state', 'date', 'keywords'];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        if (!$model->withSearch($search, $data)->count()) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->sole();
        
        return $result;
    }
    
    /**
     * 创建或更新一条数据
     *
     * @param  array  $data
     * @return array
     */
    public static function setItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        unset($data['create_time'], $data['update_time']);
        
        if (isset($data['id'])) {
            $_data = $model->withSearch(['id'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } elseif (isset($data['uuid'])) {
            $_data = $model->withSearch(['uuid'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } else {
            $_data = array_merge($data, ['uuid' => uniqid()]);
            $result['data'] = self::create($_data);
            $result['message'] = '操作成功';
        }
        
        return $result;
    }
    
    /**
     * 提交数据更新
     *
     * @param $data
     * @param  array  $updateData
     * @return array
     * @throws BadRequestHttpException
     */
    private function handleUpdate($data, array $updateData): array
    {
        if (is_null($data)) {
            throw new BadRequestHttpException('参数提交错误');
        } else {
            $is_update = self::where(['id' => $data['id']])->update($updateData);
            $result['message'] = $is_update ? '修改成功' : '修改失败';
            $result['data'] = array_merge(
                $data->toArray(),
                $updateData,
                ['is_update' => $is_update]
            );
        }
        
        return $result;
    }
    
    /**
     * 批量创建或更新数据
     *
     * @param  array  $data
     * @return array
     */
    public static function setItems(array $data): array
    {
        $result['total'] = 0;
        
        if (count($data)) {
            // 参数重组
            $_data = [];
            array_walk($data, function ($item) use (&$_data, &$result) {
                if (is_array($item)) {
                    $_data = array_merge($item, ['uuid' => uniqid()]);
                }
            });
            
            if (!count($_data)) {
                $_data = array_merge($data, ['uuid' => uniqid()]);
                $result['data'][] = self::create($_data);
            }
            
            $result['total'] = count($result['data']);
        }
        
        return $result;
    }
    
    /**
     * 批量设置数据
     *
     * @param  array  $data
     * @return array
     */
    public static function setAll(array $data): array
    {
        $result['total'] = 0;
        if (isset($data['id'])) {
            // $result['data'] = self::where()->update($data['update']);
        }
        
        return $result;
    }
    
    /**
     * 删除一个数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 删除查询：查询对象需返回
         */
        if (isset($data['id'])) {
            $data = $model->withSearch(['id'], $data)->first();
        } elseif (isset($data['uuid'])) {
            $data = $model->withSearch(['uuid'], $data)->first();
        } else {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        /**
         * 删除判断
         */
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy($data['id'])
                ])
            ];
        }
        
        return $result;
    }
    
    /**
     * 批量删除数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItems(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        if (empty($data['id'])) {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        if (is_string($data['id']) && str_contains($data['id'], ',')) {
            $data['id'] = explode(',', $data['id']);
        }
    
        $data = $model->withSearch(['ids'], $data)->get();
        
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy(array_column($data->toArray(), 'id'))
                ])
            ];
        }
        
        return $result;
    }
}
