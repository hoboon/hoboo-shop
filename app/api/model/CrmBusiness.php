<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : CrmBusiness
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-03-19 05:43:46 PM
 * @Description     : ...
 */


namespace app\api\model;

use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use hoboo\model\HobooModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;


/**
 * @method withSearch(string[] $array, array $data)
 * @method where(array $array)
 * @method static create(array|string[] $_data)
 */
class CrmBusiness extends HobooModel
{
    
    /**
     * 与模型关联的数据表.
     *
     * @var string
     */
    protected $table = 'crm_business';
    
    /**
     * 允许批量赋值的字段 白名单设置
     *
     * @var string[]
     */
    protected $fillable = [];
    
    /**
     * 不允许批量赋值的字段 黑名单设置
     *
     * @var string[]
     */
    protected $guarded = [];
    
    /**
     * @desc 数据关联：客户状态
     * @date 2022.04.09
     * @return HasOne
     */
    public function groupName(): HasOne
    {
        return $this
            ->hasOne(CrmConfig::class, 'id', 'group_id')
            ->bind([
                'group_name' => 'value'
            ]);
    }
    
    /**
     * category_uuid 局部作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeCategory(Builder $query, $data): Builder
    {
        if (isset($data['category_uuid'])) {
            return $query->where('category_uuid', $data['category_uuid']);
        }
        
        return $query;
    }
    
    /**
     * mode_uuid 局部作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeMode(Builder $query, $data): Builder
    {
        if (isset($data['mode_uuid'])) {
            return $query->where('mode_uuid', $data['mode_uuid']);
        }
        
        return $query;
    }
    
    /**
     * 获取数据列表
     * @param  array  $data
     * @return array
     * @throws
     */
    public static function getList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['state', 'group', 'keywords', 'category', 'mode'];
        
        /**
         * 初始化模型
         */
        $model = new self();
    
        /**
         * 统计查询
         */
        $data['total'] = $model->withSearch($search, $data)->count();
    
        /**
         * 分页参数预处理
         */
        $result = $model->handlePage($data);
    
        if (!$data['total']) {
            $result['data'] = [];
            return $result;
        }
    
        /**
         * 搜索器追加分页请求参数
         */
        $search[] = 'page';
        $data = array_merge($data, $result);
    
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->orderBy('id', 'desc')->get();
    
        return $result;
    }
    
    /**
     * 获取一条数据记录
     *
     * @param  array  $data
     * @return array
     */
    public static function getItem(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['uuid'];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        if (!$model->withSearch($search, $data)->count()) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->sole();
        
        return $result;
    }
    
    /**
     * 创建或更新一条数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     */
    public static function setItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        unset($data['create_time'], $data['update_time']);
        
        if (isset($data['id'])) {
            $_data = $model->withSearch(['id'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } elseif (isset($data['uuid'])) {
            $_data = $model->withSearch(['uuid'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } else {
            $_data = array_merge($data, ['uuid' => uniqid()]);
            $result['data'] = self::create($_data);
            $result['message'] = '操作成功';
        }
        
        return $result;
    }
    
    /**
     * 提交数据更新
     *
     * @param $data
     * @param  array  $updateData
     * @return array
     * @throws BadRequestHttpException
     */
    private function handleUpdate($data, array $updateData): array
    {
        if (is_null($data)) {
            throw new BadRequestHttpException('参数提交错误');
        } else {
            $is_update = self::where(['id' => $data['id']])->update($updateData);
            $result['message'] = $is_update ? '修改成功' : '修改失败';
            $result['data'] = array_merge(
                $data->toArray(),
                $updateData,
                ['is_update' => $is_update]
            );
        }
        
        return $result;
    }
    
    /**
     * 删除一个数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 删除查询：查询对象需返回
         */
        if (isset($data['id'])) {
            $data = $model->withSearch(['id'], $data)->first();
        } elseif (isset($data['uuid'])) {
            $data = $model->withSearch(['uuid'], $data)->first();
        } else {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        /**
         * 删除判断
         */
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy($data['id'])
                ])
            ];
        }
        
        return $result;
    }
    
    /**
     * 批量删除数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItems(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        if (empty($data['id'])) {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        if (is_string($data['id']) && str_contains($data['id'], ',')) {
            $data['id'] = explode(',', $data['id']);
        }
        
        $data = $model->withSearch(['ids'], $data)->get();
        
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy(array_column($data->toArray(), 'id'))
                ])
            ];
        }
        
        return $result;
    }
}
