<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : Contact
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-04-23 03:38:17 PM
 * @Description     : ...
 */


namespace app\api\model;

use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use hoboo\model\HobooModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


/**
 * @method withSearch(string[] $search, array $data)
 * @method where(array $array)
 * @method static create(array|string[] $_data)
 */
class Contact extends HobooModel
{
    
    /**
     * 与模型关联的数据表.
     *
     * @var string
     */
    protected $table = 'contact';
    
    /**
     * 允许批量赋值的字段 白名单设置
     *
     * @var string[]
     */
    protected $fillable = [];
    
    /**
     * 不允许批量赋值的字段 黑名单设置
     *
     * @var string[]
     */
    protected $guarded = [];
    
    /**
     * 数组中的属性会被隐藏。
     *
     * @var array
     */
    protected $hidden = ['delete_time'];
    
    /**
     * 类型转换
     *
     * @var array
     */
    protected $casts = [
        'create_time' => 'datetime:Y/m/d H:i:s',
        'update_time' => 'datetime:Y/m/d H:i:s',
    ];
    
    /**
     * 创建人 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeOperator(Builder $query, $data): Builder
    {
        if (isset($data['operator_uuid'])) {
            $query->where('operator_uuid', $data['operator_uuid']);
        }
        
        return $query;
    }
    
    /**
     * label    模型关联
     *
     * @return BelongsToMany
     */
    public function label(): BelongsToMany
    {
        return $this->belongsToMany(
            SystemConfig::class,// 关联模型
            'contact_label',    // 中间表
            'contact_uuid',   // 关联的模型在连接表里的外键名
            'label_uuid', // 另一个模型在连接表里的外键名
            'uuid', // 关联方法名
            'uuid'
        );
    }
    
    /**
     * group    模型关联
     *
     * @return BelongsToMany
     */
    public function group(): BelongsToMany
    {
        return $this->belongsToMany(
            SystemConfig::class,
            'contact_group',
            'contact_uuid',
            'group_uuid',
            'uuid',
            'uuid'
        );
    }
    
    /**
     * user    模型关联
     *
     * @return BelongsToMany
     */
    public function user(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'contact_hold',
            'contact_uuid',
            'user_uuid',
            'uuid',
            'uuid'
        );
    }
    
    /**
     * Organization
     *
     * @return BelongsToMany
     */
    public function organization(): BelongsToMany
    {
        return $this->belongsToMany(
            Organization::class,
            'contact_organization',
            'contact_uuid',
            'organization_uuid',
            'uuid',
            'uuid'
        );
    }
    
    /**
     * 获取数据列表
     *
     * @param  array  $data
     * @return array
     * @throws
     */
    public static function getList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['state', 'date', 'keywords', 'operator'];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        $count_model = $model->withSearch($search, $data);
        
        // 模型关联：标签
        if (isset($data['label_uuid'])) {
            $count_model->whereHas('label', function ($query) use ($data) {
                $query->where('label_uuid', $data['label_uuid']);
            });
        }
        
        // 模型关联：分组
        if (isset($data['group_uuid'])) {
            $count_model->whereHas('group', function ($query) use ($data) {
                $query->where('group_uuid', $data['group_uuid']);
            });
        }
        
        // 模型关联：用户
        if (isset($data['user_uuid'])) {
            $count_model->whereHas('user', function ($query) use ($data) {
                $query->where('user_uuid', $data['user_uuid']);
            });
        }
        
        // 模型关联：组织
        if (isset($data['organization_uuid'])) {
            $count_model->whereHas('organization', function ($query) use ($data) {
                $query->where('organization_uuid', $data['organization_uuid']);
            });
        }
        
        $data['total'] = $count_model->count();
        
        /**
         * 分页参数预处理
         */
        $result = $model->handlePage($data);
        
        if (!$data['total']) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 搜索器追加分页请求参数
         */
        $search[] = 'page';
        $data = array_merge($data, $result);
        
        /**
         * 执行查询
         */
        $data_model = $model->withSearch($search, $data);
    
        // 模型关联：标签
        if (isset($data['label_uuid'])) {
            $data_model->whereHas('label', function ($query) use ($data) {
                $query->where('label_uuid', $data['label_uuid']);
            });
        }
    
        // 模型关联：分组
        if (isset($data['group_uuid'])) {
            $data_model->whereHas('group', function ($query) use ($data) {
                $query->where('group_uuid', $data['group_uuid']);
            });
        }
    
        // 模型关联：用户
        if (isset($data['user_uuid'])) {
            $data_model->whereHas('user', function ($query) use ($data) {
                $query->where('user_uuid', $data['user_uuid']);
            });
        }
    
        // 模型关联：组织
        if (isset($data['organization_uuid'])) {
            $data_model->whereHas('organization', function ($query) use ($data) {
                $query->where('organization_uuid', $data['organization_uuid']);
            });
        }
        
        $result['data'] = $data_model->orderBy('id', 'desc')->get();
        return $result;
    }
    
    /**
     * 获取一条数据记录
     *
     * @param  array  $data
     * @return array
     */
    public static function getItem(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['uuid', 'state', 'date', 'keywords'];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        if (!$model->withSearch($search, $data)->count()) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->sole();
        
        return $result;
    }
    
    /**
     * 创建或更新一条数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     */
    public static function setItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        if (empty($data['label_uuid']) && empty($data['group_uuid'])) {
            throw new BadRequestHttpException('缺少参数');
        }
        
        // 关联新增：构建标签关联数据
        if (isset($data['label_uuid'])) {
            $contact_label_data = [
                'label_uuid' => $data['label_uuid'],
                'type'       => $data['type'] ?? 0,
                'sort'       => $data['sort'] ?? 0
            ];
        }
        
        // 关联新增：构建群组关联数据
        if (isset($data['group_uuid'])) {
            $contact_group_data = [
                'group_uuid' => $data['group_uuid'],
                'sort'       => $data['sort'] ?? 0
            ];
        }
        
        unset(
            $data['create_time'],
            $data['update_time'],
            $data['label_uuid'],
            $data['type'],
            $data['sort'],
            $data['group_uuid']
        );
        
        if (isset($data['id'])) {
            $_data = $model->withSearch(['id'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } elseif (isset($data['uuid'])) {
            $_data = $model->withSearch(['uuid'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } else {
            $_data = array_merge($data, ['uuid' => uniqid()]);
            
            if (isset($contact_label_data)) {
                // 新增 label_uuid 关联
                ContactLabel::setItem(array_merge($contact_label_data, ['contact_uuid' => $_data['uuid']]));
            }
            
            if (isset($contact_group_data)) {
                // 新增 label_uuid 关联
                ContactGroup::setItem(array_merge($contact_group_data, ['contact_uuid' => $_data['uuid']]));
            }
            
            $result['data'] = self::create($_data);
            $result['message'] = '操作成功';
        }
        
        return $result;
    }
    
    /**
     * 提交数据更新
     *
     * @param $data
     * @param  array  $updateData
     * @return array
     * @throws BadRequestHttpException
     */
    private function handleUpdate($data, array $updateData): array
    {
        if (is_null($data)) {
            throw new BadRequestHttpException('参数提交错误');
        } else {
            $is_update = self::where(['id' => $data['id']])->update($updateData);
            $result['message'] = $is_update ? '修改成功' : '修改失败';
            $result['data'] = array_merge(
                $data->toArray(),
                $updateData,
                ['is_update' => $is_update]
            );
        }
        
        return $result;
    }
    
    /**
     * 批量创建或更新数据
     *
     * @param  array  $data
     * @return array
     */
    public static function setItems(array $data): array
    {
        $result['total'] = 0;
        
        if (count($data)) {
            // 参数重组
            $_data = [];
            array_walk($data, function ($item) use (&$_data, &$result) {
                if (is_array($item)) {
                    $_data = array_merge($item, ['uuid' => uniqid()]);
                }
            });
            
            if (!count($_data)) {
                $_data = array_merge($data, ['uuid' => uniqid()]);
                $result['data'][] = self::create($_data);
            }
            
            $result['total'] = count($result['data']);
        }
        
        return $result;
    }
    
    /**
     * 批量设置数据
     *
     * @param  array  $data
     * @return array
     */
    public static function setAll(array $data): array
    {
        $result['total'] = 0;
        if (isset($data['id'])) {
            // $result['data'] = self::where()->update($data['update']);
        }
        
        return $result;
    }
    
    /**
     * 删除一个数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 删除查询：查询对象需返回
         */
        if (isset($data['id'])) {
            $data = $model->withSearch(['id'], $data)->first();
        } elseif (isset($data['uuid'])) {
            $data = $model->withSearch(['uuid'], $data)->first();
        } else {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        /**
         * 删除判断
         */
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy($data['id'])
                ])
            ];
        }
        
        return $result;
    }
    
    /**
     * 批量删除数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItems(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        if (empty($data['id'])) {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        if (is_string($data['id']) && str_contains($data['id'], ',')) {
            $data['id'] = explode(',', $data['id']);
        }
        
        $data = $model->withSearch(['ids'], $data)->get();
        
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy(array_column($data->toArray(), 'id'))
                ])
            ];
        }
        
        return $result;
    }
}
