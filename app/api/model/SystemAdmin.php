<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : SystemAdmin
 * @Author          : mr.box
 * @Createtime      : 2022/3/3 5:12 PM
 * @Description     : ...
 */

namespace app\api\model;

use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use hoboo\model\HobooModel;
use hoboo\traits\Common;
use Illuminate\Database\Eloquent\Builder;


/**
 * @method withSearch(string[] $array, array $data)
 * @method static create(array|string[] $_data)
 * @method where(string $string, mixed $account)
 */
class SystemAdmin extends HobooModel
{
    use Common;
    
    /**
     * 与模型关联的数据表.
     *
     * @var string
     */
    protected $table = 'system_admin';
    
    /**
     * 允许批量赋值的字段 白名单设置
     *
     * @var string[]
     */
    protected $fillable = [];
    
    /**
     * 不允许批量赋值的字段 黑名单设置
     *
     * @var string[]
     */
    protected $guarded = [];
    
    /**
     * 数组中的属性会被隐藏。
     *
     * @var array
     */
    protected $hidden = ['delete_time'];
    
    /**
     * 类型转换
     *
     * @var array
     */
    protected $casts = [
        'last_time'   => 'datetime:Y/m/d H:i:s',
        'create_time' => 'datetime:Y/m/d H:i:s',
        'update_time' => 'datetime:Y/m/d H:i:s',
    ];
    
    /**
     * is_sub_admin 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeSubAdmin(Builder $query, $data): Builder
    {
        if (isset($data['is_sub_admin'])) {
            $query->where('is_sub_admin', $data['is_sub_admin']);
        }
        
        return $query;
    }
    
    /**
     * role 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeRole(Builder $query, $data): Builder
    {
        if (isset($data['role_uuid'])) {
            $query->where('role_uuid', $data['role_uuid']);
        }
        
        return $query;
    }
    
    /**
     * group 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopeGroup(Builder $query, $data): Builder
    {
        if (isset($data['group_uuid'])) {
            $query->where('group_uuid', $data['group_uuid']);
        }
        
        return $query;
    }
    
    /**
     * 获取数据列表
     * @param  array  $data
     * @return array
     * @throws
     */
    public static function getList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['state', 'keywords', 'subAdmin', 'role', 'group'];
        $data['keywordsBy'] = $data['keywordsBy'] ?? 'real_name';
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        $data['total'] = $model->withSearch($search, $data)->count();
        
        /**
         * 分页参数预处理
         */
        $result = $model->handlePage($data);
        
        if (!$data['total']) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 搜索器追加分页请求参数
         */
        $search[] = 'page';
        $data = array_merge($data, $result);
        
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->orderBy('id', 'desc')->get()->makeHidden('password');
        
        return $result;
    }
    
    /**
     * 创建或更新一条数据
     *
     * @param  array  $data
     * @return array
     * @throws EmptyDataHttpException
     */
    public static function setItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 用户数据预处理
         */
        if (isset($data['password'])) {
            $data['password'] = $model->encodePassword($data['password']);
        }
        
        unset($data['create_time'], $data['update_time']);
        
        if (isset($data['id'])) {
            $_data = $model->withSearch(['id'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } elseif (isset($data['uuid'])) {
            $_data = $model->withSearch(['uuid'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } else {
            $_data = array_merge($data, ['uuid' => uniqid()]);
            $result['data'] = self::create($_data);
            $result['message'] = '操作成功';
        }
        
        return $result;
    }
    
    /**
     * 提交数据更新
     *
     * @param $data
     * @param  array  $updateData
     * @return array
     * @throws EmptyDataHttpException
     */
    private function handleUpdate($data, array $updateData): array
    {
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $is_update = self::where('id', $data['id'])->update($updateData);
            $result['message'] = $is_update ? '修改成功' : '修改失败';
            $result['data'] = array_merge(
                $data->toArray(),
                $updateData,
                ['is_update' => $is_update]
            );
        }
        
        return $result;
    }
    
    /**
     * 删除一个数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 删除查询：查询对象需返回
         */
        if (isset($data['id'])) {
            $data = $model->withSearch(['id'], $data)->first();
        } elseif (isset($data['uuid'])) {
            $data = $model->withSearch(['uuid'], $data)->first();
        } else {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        /**
         * 删除判断
         */
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy($data['id'])
                ])
            ];
        }
        
        return $result;
    }
    
    /**
     * 批量删除数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItems(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        if (empty($data['id'])) {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        if (is_string($data['id']) && str_contains($data['id'], ',')) {
            $data['id'] = explode(',', $data['id']);
        }
        
        $data = $model->withSearch(['ids'], $data)->get();
        
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy(array_column($data->toArray(), 'id'))
                ])
            ];
        }
        
        return $result;
    }
    
    /**
     * 重置一个管理员账户密码
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function resetPassword(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 用户数据预处理
         */
        if (isset($data['password'])) {
            $password = $model->encodePassword($data['password']);
        } else {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        /**
         * 数据查询
         */
        if (isset($data['id'])) {
            $_data = $model->withSearch(['id'], $data)->first();
        } elseif (isset($data['uuid'])) {
            $_data = $model->withSearch(['uuid'], $data)->first();
        } else {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        /**
         * 查询判断
         */
        if (is_null($_data)) {
            throw new EmptyDataHttpException();
        } else {
            $is_update = $model->where('id', $_data['id'])->update(['password' => $password]);
            
            return [
                'message' => '密码重置成功',
                'data'    => array_merge(
                    $_data->toArray(),
                    ['is_update' => $is_update]
                ),
            ];
        }
    }
    
    /**
     * 根据账户名称获取用户信息
     *
     * @param  array  $data
     * @return mixed
     */
    public static function getUserByAccount(array $data): mixed
    {
        $visible = ['id', 'account', 'password', 'real_name', 'head_pic', 'roles', 'login_count'];
        return (new self())
            ->where('account', $data['account'])
            ->visible($visible)
            ->first();
    }
    
    /**
     * 最后登陆信息更新
     *
     * @param  array  $data
     * @return void
     */
    public static function setLastLogin(array $data): void
    {
        $_data = [
            'last_ip'     => $data['last_ip'],
            'last_time'   => $data['last_time'],
            'login_count' => $data['login_count']
        ];
        (new SystemAdmin)->where('id', $data['id'])->update($_data);
    }
    
    /**
     * 根据角色 批量修改权限
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function setRoles(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        if (empty($data['role_uuid'])) {
            throw new BadRequestHttpException('角色参数错误');
        }
        
        if (empty($data['roles'])) {
            throw new BadRequestHttpException('权限菜单数据错误');
        }
        
        $_data = $model->withSearch(['role'], $data)->get();
        
        if (is_null($_data)) {
            throw new EmptyDataHttpException();
        }
        
        // rebuild parameter 'roles'
        $roles = implode(',', $data['roles']);
        
        // update
        $is_update = $model->where('role_uuid', $data['role_uuid'])->update(['roles' => $roles]);
    
        // rewrite parameter 'roles'
        array_walk($_data, function ($item) use (&$roles) {
            $item['roles'] = $roles;
        });
        
        return [
            'message' => $is_update ? '权限修改成功' : '权限修改失败',
            'data'    => [
                'items' => $_data->toArray(),
                'count' => $is_update
            ]
        ];
    }
}
