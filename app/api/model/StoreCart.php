<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : StoreCart
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-18 03:02:18 PM
 * @Description     : ...
 */


namespace app\api\model;

use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use hoboo\model\HobooModel;
use Illuminate\Contracts\Database\Eloquent\Builder;


/**
 * @method withSearch(string[] $array, array $data)
 * @method where(array $array)
 * @method static create(array|string[] $_data)
 */
class StoreCart extends HobooModel
{

    /**
     * 与模型关联的数据表.
     *
     * @var string
     */
    protected $table = 'store_cart';

    /**
     * 允许批量赋值的字段 白名单设置
     *
     * @var string[]
     */
    protected $fillable = [];

    /**
     * 不允许批量赋值的字段 黑名单设置
     *
     * @var string[]
     */
    protected $guarded = [];

    /**
     * 数组中的属性会被隐藏。
     *
     * @var array
     */
    protected $hidden = ['delete_time'];

    /**
     * 类型转换
     *
     * @var array
     */
    protected $casts = [
        'create_time' => 'datetime:Y/m/d H:i:s',
        'update_time' => 'datetime:Y/m/d H:i:s',
    ];


    public function scopeUid(Builder $query,array $data):Builder
    {
        if(isset($data["uid"])){
            $query->where("uid",$data["uid"]);
        }
        return $query;

    }
    public function scopeProductId(Builder $query,array $data):Builder
    {
        if(isset($data["product_id"])){
            $query->where("product_id",$data["product_id"]);
        }
        return $query;
    }

    /**
     * 获取数据列表
     *
     * @param  array  $data
     * @return array
     * @throws
     */
    public static function getList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['uid','state', 'date', 'keywords',"id","is_order_confirm"];

        /**
         * 初始化模型
         */
        $model = new self();

        /**
         * 统计查询
         */
        $data['total'] = $model->withSearch($search, $data)->count();

        /**
         * 分页参数预处理
         */
        $result = $model->handlePage($data);

        if (!$data['total']) {
            $result['data'] = [];
            return $result;
        }

        /**
         * 搜索器追加分页请求参数
         */
        $search[] = 'page';
        $data = array_merge($data, $result);
//        dd($data,$search);
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->orderBy('id', 'desc')->get();

        return $result;
    }

    /**
     * 获取一条数据记录
     *
     * @param  array  $data
     * @return array
     */
    public static function getItem(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['uuid',"uid","product_id", 'state', 'date', 'keywords'];

        /**
         * 初始化模型
         */
        $model = new self();

        /**
         * 统计查询
         */
        if (!$model->withSearch($search, $data)->count()) {
            $result['data'] = [];
            return $result;
        }

        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->sole();

        return $result;
    }

    /**
     * 创建或更新一条数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     */
    public static function setItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();

        unset($data['create_time'], $data['update_time']);

        if (isset($data['id'])) {
            $_data = $model->withSearch(['id'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } elseif (isset($data['uuid'])) {
            $_data = $model->withSearch(['uuid'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } else {
            $_data = array_merge($data, ['uuid' => uniqid()]);
            $result['data'] = self::create($_data);
            $result['message'] = '操作成功';
        }

        return $result;
    }
    public static function setCart(array $data):array
    {
        $type="product";
        //判断是否是新加入还是购物车内有的
//        dd($data);
        if (isset($data['is_new']) || isset($data['new'])) $new = true;
        else $new = false;

        if (!$data['product_id'] || !is_numeric($data['product_id'])){
            throw new BadRequestHttpException('参数提交错误');
        }
        //商品类
        $storeProduct=new StoreProduct();
//        [$res, $product_attr_unique, $bargainPriceMin, $cart_num]=checkProductStock
//        $checkData=self::checkProductStock($data,$new);
        if($new){
//            $key=self::getCartId($data["uid"]);
            $key=time();
            $info["id"]=$key;
            $info["type"]=$type;
            $info["product_id"]=$data["product_id"];
            $info["cart_num"]=$data["cart_num"];

            //预留，是否为拼团商品
            if(!empty($data["combination_id"])){

            }else{
                $where=[
                    "id"=>$data["product_id"]
                ];
                $product=$storeProduct::getItem($where);
                $info["productInfo"]=$product["data"];
            }
            //属性值暂时不做
//            $info['productInfo']['attrInfo'] ;

            //为砍价功能模块做字段预留
//            $info["truePrice"]=$info["productInfo"]["price"];

            //为vip会员系统作字段预留
//            $info["vip_truePrice"]=0;
//            dd($info);

            try {
                //为存入redis作预留
                //暂时先做存入数据库操作
                $add_data=[
                    "uid"=>$data["uid"],
                    "type"=>$type,
                    "product_id"=>$data["product_id"],
                    "cart_num"=>1,
                    "is_pay"=>0,
                    "is_new"=>0,
                ];
                return self::setItem($add_data);
            }catch (\Throwable $e){

            }

        }else{
            //加入购物车记录
            $where=[
                "uid"=>$data["uid"],
                "product_id"=>$data["product_id"],
                //其他判断条件后期添加
            ];

            $cart=self::getItem($where);

            if(!empty($cart["data"])){
                $cartData=$cart["data"];
                $cartData["cart_num"]=$data["cart_num"]+$cartData["cart_num"];
                $cartData["update_time"]=time();

                $cart=self::setItem($cartData->toArray());

                return $cart;
            }else{
                //未做完善
                return self::setItem($data);
            }
        }
    }

    /**
     * 使用雪花算法生成订单ID
     */
    public static function getCartId($prefix)
    {
        $snowflake = new \Godruoyi\Snowflake\Snowflake();
        //32位
        if (PHP_INT_SIZE == 4) {
            $id = abs($snowflake->id());
        } else {
            $id = $snowflake->setStartTimeStamp(strtotime('2022-05-30') * 1000)->id();
        }
        return $prefix . $id;
    }
    public function checkProductStock()
    {

    }

    /**
     * 提交数据更新
     *
     * @param $data
     * @param  array  $updateData
     * @return array
     * @throws BadRequestHttpException
     */
    private function handleUpdate($data, array $updateData): array
    {
        if (is_null($data)) {
            throw new BadRequestHttpException('参数提交错误');
        } else {
            $is_update = self::where(['id' => $data['id']])->update($updateData);
            $result['message'] = $is_update ? '修改成功' : '修改失败';
            $result['data'] = array_merge(
                $data->toArray(),
                $updateData,
                ['is_update' => $is_update]
            );
        }

        return $result;
    }

    /**
     * 批量创建或更新数据
     *
     * @param  array  $data
     * @return array
     */
    public static function setItems(array $data): array
    {
        $result['total'] = 0;

        if (count($data)) {
            // 参数重组
            $_data = [];
            array_walk($data, function ($item) use (&$_data, &$result) {
                if (is_array($item)) {
                    $_data = array_merge($item, ['uuid' => uniqid()]);
                }
            });

            if (!count($_data)) {
                $_data = array_merge($data, ['uuid' => uniqid()]);
                $result['data'][] = self::create($_data);
            }

            $result['total'] = count($result['data']);
        }

        return $result;
    }

    /**
     * 批量设置数据
     *
     * @param  array  $data
     * @return array
     */
    public static function setAll(array $data): array
    {
        $result['total'] = 0;
        if (isset($data['id'])) {
            // $result['data'] = self::where()->update($data['update']);
        }

        return $result;
    }

    /**
     * 删除一个数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();

        /**
         * 删除查询：查询对象需返回
         */
        if (isset($data['id'])) {
            $data = $model->withSearch(['id'], $data)->first();
        } elseif (isset($data['uuid'])) {
            $data = $model->withSearch(['uuid'], $data)->first();
        } else {
            throw new BadRequestHttpException('参数提交错误');
        }

        /**
         * 删除判断
         */
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy($data['id'])
                ])
            ];
        }

        return $result;
    }

    /**
     * 批量删除数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItems(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();

        if (empty($data['id'])) {
            throw new BadRequestHttpException('参数提交错误');
        }

        if (is_string($data['id']) && str_contains($data['id'], ',')) {
            $data['id'] = explode(',', $data['id']);
        }

        $data = $model->withSearch(['ids'], $data)->get();

        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy(array_column($data->toArray(), 'id'))
                ])
            ];
        }

        return $result;
    }
}
