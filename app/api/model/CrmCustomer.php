<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : CrmCustomer
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-03-19 05:43:46 PM
 * @Description     : ...
 */


namespace app\api\model;

use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use hoboo\model\HobooModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;


/**
 * @method withSearch(string[] $array, array $data)
 * @method static create(array|string[] $_data)
 * @method where(string $string, mixed $id)
 */
class CrmCustomer extends HobooModel
{
    
    /**
     * 与模型关联的数据表.
     *
     * @var string
     */
    protected $table = 'crm_customer';
    
    /**
     * 允许批量赋值的字段 白名单设置
     *
     * @var string[]
     */
    protected $fillable = [
        'uuid',
        'industry_uuid',
        'category_uuid',
        'source_uuid',
        'label_uuid',
        'level_uuid',
        'stage_uuid',
        'status_uuid',
        'name',
        'contact',
        'phone',
        'qq',
        'wechat',
        'salesman',
        'contents',
        'last_follow_time',
        'next_plan_time',
        'remarks',
        'state',
    ];
    
    /**
     * 类型转换
     *
     * @var array
     */
    protected $casts = [
        'last_follow_time' => 'datetime:Y/m/d H:i:s',
        'next_plan_time'   => 'datetime:Y/m/d H:i:s',
        'create_time'      => 'datetime:Y/m/d H:i:s',
        'update_time'      => 'datetime:Y/m/d H:i:s',
    ];
    
    /**
     * 不允许批量赋值的字段 黑名单设置
     *
     * @var string[]
     */
    protected $guarded = [];
    
    /**
     * @desc 数据关联：客户联系人
     * @date 2022.04.01
     * @return HasOne
     */
    public function contacts(): HasOne
    {
        return $this
            ->hasOne(CrmCustomerContacts::class, 'customer_uuid', 'uuid')
            ->bind([
                'contacts_name' => 'name',
                'contacts_uuid' => 'uuid',
            ]);
    }
    
    /**
     * @desc 数据关联：客户来源
     * @date 2022.04.09
     * @return HasOne
     */
    public function source(): HasOne
    {
        return $this
            ->hasOne(CrmConfig::class, 'id', 'source_id')
            ->bind([
                'source' => 'value'
            ]);
    }
    
    /**
     * @desc 数据关联：客户等级
     * @date 2022.04.09
     * @return HasOne
     */
    public function grade(): HasOne
    {
        return $this
            ->hasOne(CrmConfig::class, 'id', 'grade_id')
            ->bind([
                'grade' => 'value'
            ]);
    }
    
    /**
     * @desc 数据关联：客户状态
     * @date 2022.04.09
     * @return HasOne
     */
    public function state(): HasOne
    {
        return $this
            ->hasOne(CrmConfig::class, 'id', 'state_id')
            ->bind([
                'state' => 'value'
            ]);
    }
    
    /**
     * @desc 数据关联：当前阶段
     * @date 2022.04.09
     * @return HasOne
     */
    public function currentStage(): HasOne
    {
        return $this
            ->hasOne(CrmConfig::class, 'id', 'current_stage_id')
            ->bind([
                'current_stage' => 'value'
            ]);
    }
    
    /**
     * salesman 局部作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeSalesman(Builder $query, $data): Builder
    {
        if (isset($data['salesman'])) {
            return $query->where('salesman', $data['salesman']);
        }
        
        return $query;
    }
    
    /**
     * stage 局部作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeStage(Builder $query, $data): Builder
    {
        if (isset($data['stage_uuid'])) {
            return $query->where('stage_uuid', $data['stage_uuid']);
        }
        
        return $query;
    }
    
    /**
     * source 局部作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeSource(Builder $query, $data): Builder
    {
        if (isset($data['source_uuid'])) {
            return $query->where('source_uuid', $data['source_uuid']);
        }
        
        return $query;
    }
    
    /**
     * category 局部作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeCategory(Builder $query, $data): Builder
    {
        if (isset($data['category_uuid'])) {
            return $query->where('category_uuid', $data['category_uuid']);
        }
        
        return $query;
    }
    
    /**
     * industry 局部作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeIndustry(Builder $query, $data): Builder
    {
        if (isset($data['industry_uuid'])) {
            return $query->where('industry_uuid', $data['industry_uuid']);
        }
        
        return $query;
    }
    
    /**
     * label 局部作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeLabel(Builder $query, $data): Builder
    {
        if (isset($data['label_uuid'])) {
            return $query->where('label_uuid', $data['label_uuid']);
        }
        
        return $query;
    }
    
    /**
     * status 局部作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeStatus(Builder $query, $data): Builder
    {
        if (isset($data['status_uuid'])) {
            $query->where('status_uuid', $data['status_uuid']);
        } else {
            $query->where('status_uuid', '!=', 0);
        }
        
        return $query;
    }
    
    /**
     * 获取数据列表
     *
     * @param  array  $data
     * @return array
     * @throws
     */
    public static function getList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = [
            'state',    // 作用域：数据状态
            'date',     // 作用域：时间参数
            'keywords', // 作用域：搜索关键词
            'salesman', // 作用域：销售联系人
            'source',   // 作用域：客户来源
            'stage',    // 作用域：销售阶段
            'category', // 作用域：客户分类
            'industry', // 作用域：客户行业
            'label',    // 作用域：客户标签
            'status',   // 作用域：客户状态
        ];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        $data['total'] = $model->withSearch($search, $data)->count();
        
        /**
         * 分页参数预处理
         */
        $result = $model->handlePage($data);
        
        if (!$data['total']) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 搜索器追加分页请求参数
         */
        $search[] = 'page'; // 作用域：分页请求
        $data = array_merge($data, $result);
        
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->orderBy('id', 'desc')->get();
        
        return $result;
    }
    
    /**
     * 获取一条数据记录
     *
     * @param  array  $data
     * @return array
     */
    public static function getItem(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['uuid'];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        if (!$model->withSearch($search, $data)->count()) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->sole();
        
        return $result;
    }
    
    /**
     * 创建或更新一条数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     */
    public static function setItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        unset($data['create_time'], $data['update_time']);
        
        if (isset($data['id'])) {
            $_data = $model->withSearch(['id'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } elseif (isset($data['uuid'])) {
            $_data = $model->withSearch(['uuid'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } else {
            $_data = array_merge($data, ['uuid' => uniqid()]);
            $result['data'] = self::create($_data);
            $result['message'] = '操作成功';
        }
        
        return $result;
    }
    
    /**
     * 提交数据更新
     *
     * @param $data
     * @param  array  $updateData
     * @return array
     * @throws BadRequestHttpException
     */
    private function handleUpdate($data, array $updateData): array
    {
        if (is_null($data)) {
            throw new BadRequestHttpException('参数提交错误');
        } else {
            $is_update = self::where('id', $data['id'])->update($updateData);
            $result['message'] = $is_update ? '修改成功' : '修改失败';
            $result['data'] = array_merge(
                $data->toArray(),
                $updateData,
                ['is_update' => $is_update]
            );
        }
        
        return $result;
    }
    
    /**
     * 批量创建或更新数据
     *
     * @param  array  $data
     * @return array
     */
    public static function setItems(array $data): array
    {
        $result['total'] = 0;
        
        if (count($data)) {
            // 参数重组
            $_data = [];
            array_walk($data, function ($item) use (&$_data, &$result, &$model) {
                if (is_array($item)) {
                    $_data = array_merge($item, ['uuid' => uniqid()]);
                    // $result['data'][] = self::create($_data);
                }
            });
            
            if (!count($_data)) {
                $_data = array_merge($data, ['uuid' => uniqid()]);
                $result['data'][] = self::create($_data);
            }
            
            $result['total'] = count($result['data']);
        }
        
        return $result;
    }
    
    /**
     * 批量设置数据
     *
     * @param  array  $data
     * @return array
     */
    public static function setAll(array $data): array
    {
        $result['total'] = 0;
        if (isset($data['id'])) {
            // $result['data'] = self::where()->update($data['update']);
        }
        
        return $result;
    }
    
    /**
     * 删除一个数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 删除查询：查询对象需返回
         */
        if (isset($data['id'])) {
            $data = $model->withSearch(['id'], $data)->first();
        } elseif (isset($data['uuid'])) {
            $data = $model->withSearch(['uuid'], $data)->first();
        } else {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        /**
         * 删除判断
         */
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy($data['id'])
                ])
            ];
        }
        
        return $result;
    }
    
    /**
     * 批量删除数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItems(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        if (empty($data['id'])) {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        if (is_string($data['id']) && str_contains($data['id'], ',')) {
            $data['id'] = explode(',', $data['id']);
        }
        
        $data = $model->withSearch(['ids'], $data)->get();
        
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy(array_column($data->toArray(), 'id'))
                ])
            ];
        }
        
        return $result;
    }
    
    /**
     * 跟进客户ID获取信息
     *
     * @param  array  $data
     * @return mixed
     */
    public static function getCustomerById(array $data): mixed
    {
        return (new self())->where('id', $data['id'])->first();
    }
}
