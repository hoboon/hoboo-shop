<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : SystemMenus
 * @Author          : mr.box
 * @Createtime      : 2022/3/3 5:12 PM
 * @Description     : ...
 */

namespace app\api\model;

use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use hoboo\model\HobooModel;
use Illuminate\Database\Eloquent\Builder;


/**
 * @method withSearch(string[] $array, array $data)
 * @method where(array $array)
 * @method static create(array|string[] $_data)
 * @method static count()
 */
class SystemMenus extends HobooModel
{
    
    /**
     * 与模型关联的数据表.
     *
     * @var string
     */
    protected $table = 'system_menu';
    
    /**
     * 允许批量赋值的字段 白名单设置
     *
     * @var string[]
     */
    protected $fillable = [
        'uuid',
        'pid',
        'sys_module',
        'icon',
        'name',
        'module',
        'api_url',
        'sort',
        'is_show',
        'path',
        'header',
        'is_header'
    ];
    
    /**
     * 不允许批量赋值的字段 黑名单设置
     *
     * @var string[]
     */
    protected $guarded = [];
    
    /**
     * path 作用域
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    public function scopePath(Builder $query, $data): Builder
    {
        if (isset($data['path'])) {
            $query->where('path', $data['path']);
        }
        
        return $query;
    }
    
    /**
     * 获取数据列表
     *
     * @param  array  $data
     * @return array
     */
    public static function getList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['state', 'ids', 'keywords'];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 统计查询
         */
        $data['total'] = $model->withSearch($search, $data)->count();
        
        /**
         * 分页参数预处理
         */
        $result = $model->handlePage($data);
        
        if (!$data['total']) {
            $result['data'] = [];
            return $result;
        }
        
        /**
         * 搜索器追加分页请求参数
         */
        $search[] = 'page';
        $data = array_merge($data, $result);
        
        /**
         * 执行查询
         */
        $result['data'] = $model->withSearch($search, $data)->orderBy('id', 'desc')->get();
        
        /**
         * 非分页查询场景下，结果去重
         */
        if (!isset($data['page'])) {
            $result['data'] = array_values(array_column($result['data']->toArray(), null, 'path'));
        }
        
        return $result;
    }
    
    /**
     * 创建或更新一条数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     */
    public static function setItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        unset($data['create_time'], $data['update_time']);
        
        if (isset($data['id'])) {
            $_data = $model->withSearch(['id'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } elseif (isset($data['uuid'])) {
            $_data = $model->withSearch(['uuid'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } elseif (isset($data['path'])) {
            $_data = $model->withSearch(['path'], $data)->first();
            $result = $model->handleUpdate($_data, $data);
        } else {
            $_data = array_merge($data, ['uuid' => uniqid()]);
            $result['data'] = self::create($_data);
            $result['message'] = '新增成功';
        }
        
        return $result;
    }
    
    /**
     * 提交数据更新
     *
     * @param $data
     * @param  array  $updateData
     * @return array
     * @throws BadRequestHttpException
     */
    private function handleUpdate($data, array $updateData): array
    {
        if (is_null($data)) {
            $_data = array_merge($updateData, ['uuid' => uniqid()]);
            $result['data'] = self::create($_data);
            $result['message'] = '新增成功';
        } else {
            $is_update = self::where(['id' => $data['id']])->update($updateData);
            $result['message'] = $is_update ? '修改成功' : '修改失败';
            $result['data'] = array_merge(
                $data->toArray(),
                $updateData,
                ['is_update' => $is_update]
            );
        }
        
        return $result;
    }
    
    /**
     * 删除一个数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItem(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        /**
         * 删除查询：查询对象需返回
         */
        if (isset($data['id'])) {
            $data = $model->withSearch(['id'], $data)->first();
        } elseif (isset($data['uuid'])) {
            $data = $model->withSearch(['uuid'], $data)->first();
        } else {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        /**
         * 删除判断
         */
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy($data['id'])
                ])
            ];
        }
        
        return $result;
    }
    
    /**
     * 批量删除数据
     *
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public static function delItems(array $data): array
    {
        /**
         * 初始化模型
         */
        $model = new self();
        
        if (empty($data['id'])) {
            throw new BadRequestHttpException('参数提交错误');
        }
        
        if (is_string($data['id']) && str_contains($data['id'], ',')) {
            $data['id'] = explode(',', $data['id']);
        }
        
        $data = $model->withSearch(['ids'], $data)->get();
        
        if (is_null($data)) {
            throw new EmptyDataHttpException();
        } else {
            $result = [
                'message' => '删除成功',
                'data'    => array_merge($data->toArray(), [
                    'is_delete' => $model->destroy(array_column($data->toArray(), 'id'))
                ])
            ];
        }
        
        return $result;
    }
    
    /**
     * 批量新增
     *
     * @date 2022.04.06
     * @param    $data
     * @return array
     */
    public static function initialization($data): array
    {
        $result['total'] = 0;
        
        if (count($data)) {
            // 参数重组
            array_walk($data, function ($item) use (&$_data, &$result) {
                if (is_array($item)) {
                    $_item = (new SystemMenus)->withSearch(['path'], $item)->first();
                    if (is_null($_item)) {
                        $_data = array_merge($item, ['uuid' => uniqid()]);
                        $result['data'][] = (new SystemMenus)->create($_data);
                    } else {
                        $is_update = (new SystemMenus)->where(['id' => $_item['id']])->update($item);
                        $result['data'][] = array_merge(
                            $_item->toArray(),
                            $item,
                            ['is_update' => $is_update]
                        );
                    }
                }
            });
            
            $result['total'] = count($result['data']);
        }
        
        return $result;
    }
    
    /**
     * 获取一个菜单配置
     * @param  array  $data
     * @return array
     */
    public static function getMenuList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['page'];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        $result['total'] = SystemMenus::count();
        
        if (!$data['total'] = $model->withSearch($search, $data)->count()) {
            $result['data'] = [];
            $result['payload'] = $data; // 测试
            return $result;
        }
        
        /**
         * 分页参数预处理
         */
        $result = $model->handlePage($data);
        $data = array_merge($data, $result);
        
        
        /**
         * 执行查询
         */
        $list = $model->withSearch($search, $data)->get()->toArray();
        
        $result['data'] = $model->handleMenus($list);   // 菜单树
        
        return $result;
    }
    
    /**
     * 获取指定编号数据列表
     *
     * @param  array  $data
     * @return array
     */
    public static function getAuthList(array $data): array
    {
        /**
         * 条件查询预处理
         */
        $search = ['ids'];
        
        /**
         * 初始化模型
         */
        $model = new self();
        
        if (!$data['total'] = $model->withSearch($search, $data)->count()) {
            $result['data'] = [];
            return $result;
        }
        
        // 指定显示字段
        $visible = ['id', 'name', 'path', 'sort'];
        
        $result['data'] = $model
            ->withSearch($search, $data)
            ->visible($visible)
            ->orderBy('id', 'desc')
            ->get();
        
        return $result;
    }
    
    /**
     * 菜单树形结构处理
     * @param  array  $data
     * @param  int  $pid
     * @param  int  $level
     * @return array
     */
    private static function handleMenus(array $data, int $pid = 0, int $level = 1): array
    {
        $_list = [];
        
        array_walk($data, function ($item, $key) use (&$_list, $data, $pid, $level) {
            if ($item['pid'] === $pid) {
                $item['level'] = $level;
                $item['label'] = $item['name'];
                $item['children'] = self::handleMenus($data, $item['id'], $level + 1);
                
                unset($item['module']);
                unset($item['create_time']);
                unset($item['update_time']);
                
                $_list[] = $item;   // 存储节点
                unset($data[$key]); // 删除节点，减少递归消耗
            }
        });
        
        return $_list;
    }
    
    /**
     * 字段名称转换
     *
     * @date 2022-03-17
     * @param  array  $data
     * @return array
     */
    private function handleMenuListName(array $data): array
    {
        $_list = [];
        
        array_walk($data, function ($item) use (&$_list) {
            $item['label'] = $item['name'];
            unset($item['module']);
            $_list[] = $item;
        });
        
        return $_list;
    }
}
