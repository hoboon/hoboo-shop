<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : StoreCouponUser
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-18 03:02:17 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\StoreCouponUser as Model;
use app\api\model\StoreCouponIssueUser;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class StoreCouponUser extends HobooController
{

    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));
        $couponData=$result["data"];
        foreach ($couponData as &$item){
            $couponIssue=\app\api\model\StoreCouponIssue::getItem(["id"=>$item["cid"]]);
            $couponIssueData=$couponIssue["data"];
            $item["store_coupon_issue"]=$couponIssueData;
        }
//        dd($couponData);
        $result["data"]=$couponData;
        return $this->success($result);
    }

    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $params=$this->getData($request);
        $where=[
            "cid"=>$params["cid"],
            "uid"=>$params["uid"]
        ];
        $couponResult=Model::getItem($where);
        $couponData=$couponResult["data"];
        if(!empty($couponData)){
            return $this->fail("该优惠券已领取");
        }
        $result = Model::setItem($params);
        //添加完用户领取优惠券信息后，再把uid与优惠券id放入中间关联表
        $data=$result["data"];
        $add_data=[
            "uid"=>$data["uid"],
            "issue_coupon_id"=>$data["cid"]
        ];
        $result=StoreCouponIssueUser::setItem($add_data);
        return $this->success($result);
    }

    /**
     * 批量创建或更新数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {
        $result = Model::setItems($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量设置数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setAll(Request $request): Response
    {
        $result = Model::setAll($this->getData($request));

        return $this->success($result);
    }

    /**
     * 删除一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));

        return $this->success($result);
    }
}
