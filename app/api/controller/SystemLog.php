<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : SystemLog
 * @Author          : mr.box
 * @Createtime      : 2022/3/3 2:19 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\SystemLog as Model;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class SystemLog extends HobooController
{
    
    
    /**
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 创建或更新一个日志信息
     * @param  Request  $request
     * @return Response
     */
    public function setItem(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 删除一个日志信息
     * @param  Request  $request
     * @return Response
     */
    public function delItem(Request $request): Response
    {
        $data = $request->post();
        
        $result = Model::delItem($data);
        
        return $this->success($result);
    }
}
