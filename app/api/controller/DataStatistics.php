<?php


namespace app\api\controller;


use app\service\DataStatisticsService;
use hoboo\controller\HobooController;
use support\Request;
use support\Response;
use app\api\model\StoreOrder;

class DataStatistics extends HobooController
{
    //顶部统计
    public function topStatistics(Request $request):Response
    {
        $dataStatisticService=new DataStatisticsService();
        //初始化数据，获取销售额
        $data=$dataStatisticService->topCardSalesStatistic();
        //获取用户访问量
        $data[1]=$dataStatisticService->userVisitData();
        //获取订单量
        $data[2]=$dataStatisticService->orderCountStatistic();
        //新增用户统计
        $data[3]=$dataStatisticService->newUserCount();
        $result["data"]=$data;
//        $result["data"]=$data;
//        dd(json_encode($data));
//        return json($data);
        return $this->success($result);
    }
    //中部交易数据统计
    public function middleStatistic(Request $request):Response
    {
        $dataStatisticService=new DataStatisticsService();
        $data[0]=$dataStatisticService->unPaidOrderCount();
        $data[1]=$dataStatisticService->unDeliverOrderCount();
        $data[2]=$dataStatisticService->unReceiveOrderCount();
        $data[3]=$dataStatisticService->afterSaleCount();
        $result["data"]=$data;
        return $this->success($result);
    }

}