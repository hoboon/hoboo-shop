<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : StoreProduct
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-18 03:02:16 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\StoreProduct as Model;
use app\api\model\StoreProductDescription;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class StoreProduct extends HobooController
{
    
    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));
        $pp=$result["data"];

        foreach ($pp as &$item){
            //获取商品详情
            $where=[
                "product_id"=>$item["id"]
            ];
            $storeProductDescription=new StoreProductDescription();
            $data=$storeProductDescription->getItem($where);

            if(empty($data["data"])){
                $item["description"]="";
            }else{
                $item["description"]=$data["data"]["description"];
            }
        }
        $result["data"]=$pp;
        return $this->success($result);
    }
    
    /**
     * 获取一个已删除数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getDelList(Request $request): Response
    {
        $result = Model::getDelList($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));
        $storeProductDescription = new StoreProductDescription();
        $where = [
            'product_id' => $result['data']['id']
        ];
        //获取商品详情
        $data = $storeProductDescription->getItem($where);
        if (empty($data['data'])) {
            unset($result['data']['description']);
        } else {
            $result['data']['description'] = $data['data']['description'];
        }
        //处理轮播图
        $productData = $result['data'];
        
        $result['data'] = $productData;
        return $this->success($result);
    }

    public function setSimpleItem(Request $request): Response
    {
        $data = $this->getData($request);
        $result = Model::setItem($data);
        return $this->success($result);
    }

    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $data = $this->getData($request);
        
        $result = $this->setStoreProduct($data);
        if(!is_array($result['data'])){
            $result['data'] =$result['data']->toArray();
        }

        $result['data'] = array_merge($result['data'], $this->setStoreProductDescription($data));

        return $this->success($result);
    }
    public function setBrowser(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));

        return $this->success($result);
    }
    
    /**
     * 批量创建或更新数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {
        $result = Model::setItems($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 批量设置数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setAll(Request $request): Response
    {
        $result = Model::setAll($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 删除一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * @desc 商品基础信息保存
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     */
    private function setStoreProduct(array $data): array
    {
        // 重组商品基础信息数据
        $_data = [
            'id'              => $data['id'] ?? 0,
            'uuid'            => $data['uuid'] ?? '',
            'mer_id'          => $data['mer_id'] ?? 0,
            'image'           => $data['image'] ?? '',
            'recommend_image' => $data['recommend_image'] ?? '',
            'slider_image'    => $data['slider_image'] ?? '',
            'store_name'      => $data['store_name'] ?? '',
            'store_info'      => $data['store_info'] ?? '',
            'keyword'         => $data['keyword'] ?? '',
            'bar_code'        => $data['bar_code'] ?? '',
            'price'           => $data['price'] ?? 0.00,
            'vip_price'       => $data['vip_price'] ?? 0.00,
            'ot_price'        => $data['ot_price'] ?? 0.00,
            'postage'         => $data['postage'] ?? 0.00,
            'unit_name'       => $data['unit_name'],
            'sort'            => $data['sort'] ?? 0,
            'sales'           => $data['sales'] ?? 0,
            'stock'           => $data['stock'] ?? 0,
            'is_show'         => $data['is_show'] ?? '1',
            'is_hot'          => $data['is_hot'] ?? '0',
            'is_benefit'      => $data['is_benefit'] ?? '0',
            'is_best'         => $data['is_best'] ?? '0',
            'is_new'          => $data['is_new'] ?? '0',
            'is_postage'      => $data['is_postage'] ?? '0',
            'mer_use'         => $data['mer_use'] ?? '0',
            'give_integral'   => $data['give_integral'] ?? 0.00,
            'cost'            => $data['cost'] ?? 0.00,
            'is_seckill'      => $data['is_seckill'] ?? '0',
            'is_bargain'      => $data['is_bargain'] ?? '0',
            'is_good'         => $data['is_good'] ?? '0',
            'is_sub'          => $data['is_sub'] ?? '0',
            'is_vip'          => $data['is_vip'] ?? '0',
            'ficti'           => $data['ficti'] ?? 100,
            'browse'          => $data['browse'] ?? 0,
            'code_path'       => $data['code_path'] ?? '',
            'soure_link'      => $data['soure_link'] ?? '',
            'video_link'      => $data['video_link'] ?? '',
            'temp_id'         => $data['temp_id'] ?? 1,
            'spec_type'       => $data['spec_type'] ?? '0',
            'activity'        => $data['activity'] ?? '',
            'spu'             => $data['spu'] ?? '',
            'label_id'        => $data['label_id'] ?? '',
            'command_word'    => $data['command_word'] ?? '',
            'state'           => $data['state'] ?? 1,
            'brand_id'        => $data['brand_id'],
            'cate_id'         => $data['cate_id'],
        ];
        if(!empty($_data["slider_image"])){
            $_data["image"]=$_data["slider_image"][0];
        }
        
        return Model::setItem($_data);
    }
    
    /**
     * @desc 商品详情保存
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     */
    private function setStoreProductDescription(array $data): array
    {
        // 重组商品详情数据
        $_data = [
            'product_id'  => $data['id'] ?? 0,
            'description' => $data['description'] ?? '',
            'type'        => $data['type'] ?? 0,
            'state'       => $data['state'] ?? 1,
        ];
        $result=StoreProductDescription::getItem(["product_id"=>$_data["product_id"]]);
        $resultData=$result["data"];
        if(empty($resultData)){
            $addResult=StoreProductDescription::insert($_data);
        }else{
            $addResult=StoreProductDescription::where(["product_id"=>$_data["product_id"]])->update($_data);
        }
        if($addResult){
            $result=StoreProductDescription::getItem(["product_id"=>$_data["product_id"]]);
            $resultData=$result["data"];
            unset($resultData["uuid"]);
            return $resultData->toArray();
        }else{
            return [];
        }

    }
}
