<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : WechatUser
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-18 03:02:13 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\WechatUser as Model;
use EasyWeChat\Factory;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class WechatUser extends HobooController
{

    public function getOpenId(Request $request): Response
    {
        $params=$this->getData($request);

        $app_id=config("wechatmini.app_id");
        $app_secret=config("wechatmini.app_secret");
        $config=[
            "app_id"=>$app_id,
            "secret"=>$app_secret,
            'response_type' => 'array',
            'log' => [
                'level' => 'debug',
                'file' => __DIR__.'/wechat.log',
            ],
        ];
        $app=Factory::miniProgram($config);

        $userSession=$app->auth->session($params["code"]);
//        dd($userSession);
        $openId=$userSession["openid"];
        //查询数据库是否有openid
        $wechatResult=Model::getItem(["openid"=>$openId]);
        $wechatData=$wechatResult["data"];
        if(empty($wechatData)){
            //创建新纪录
            $wechatResult=Model::setItem(["openid"=>$openId]);
        }
        //直接返回记录
        return $this->success($wechatResult);
    }
    public function updateWechatUser(Request $request): Response
    {
        $params=$this->getData($request);
        $result=Model::setItem($params);
        $wechatData=$result["data"];
        if($wechatData["uid"]==0){
            //未绑定用户，则创建用户
            $add_data=[
                "nickname"=>$wechatData["nickname"],
                "avatar"=>$wechatData["headimgurl"]
            ];
            $userResult=\app\api\model\User::setItem($add_data);
            $userData=$userResult["data"];
            $userData["phone"]=trim($userData["phone"]);
            $userResult["data"]=$userData;
            //更新wechat表
            $update_data=[
                "id"=>$wechatData["id"],
                "uid"=>$userData["id"]
            ];
            $result=Model::setItem($update_data);
        }else{
            $userResult=\app\api\model\User::getItem(["id"=>$wechatData["uid"]]);
            $userData=$userResult["data"];
            $userData["phone"]=trim($userData["phone"]);
            $userResult["data"]=$userData;
        }
        return $this->success($userResult);

    }

    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));

        return $this->success($result);
    }

    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));


        return $this->success($result);
    }

    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量创建或更新数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {
        $result = Model::setItems($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量设置数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setAll(Request $request): Response
    {
        $result = Model::setAll($this->getData($request));

        return $this->success($result);
    }

    /**
     * 删除一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));

        return $this->success($result);
    }
}
