<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : SystemAdmin
 * @Author          : mr.box
 * @Createtime      : 2022/3/3 2:19 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\SystemAdmin as Model;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class SystemAdmin extends HobooController
{
    
    
    /**
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 创建或更新一个管理员用户
     * @param  Request  $request
     * @return Response
     * @throws EmptyDataHttpException
     */
    public function setItem(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 删除一个管理员用户
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 重置一个管理员账户密码
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function resetPassword(Request $request): Response
    {
        $result = Model::resetPassword($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 根据角色 批量修改权限
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function setRoles(Request $request): Response
    {
        $result = Model::setRoles($this->getData($request));
        
        return $this->success($result);
    }
}
