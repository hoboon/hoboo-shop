<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : StoreCouponIssue
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-18 03:02:17 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\StoreCouponIssue as Model;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class StoreCouponIssue extends HobooController
{

    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));


        return $this->success($result);
    }

    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $params=$this->getData($request);
        if(isset($params["start_use_time"]) ){
            $params["start_use_time"]=strtotime($params["start_use_time"]);
        }
        if(isset($params["end_use_time"]) ){
            $params["end_use_time"]=strtotime($params["end_use_time"]);
        }
        if(isset($params["start_time"] ) ){
            $params["start_time"]=strtotime($params["start_time"]);
        }
        if(isset($params["end_time"]) ){
            $params["end_time"]=strtotime($params["end_time"]);
        }

        $result = Model::setItem($params);

        return $this->success($result);
    }

    /**
     * 批量创建或更新数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {
        $result = Model::setItems($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量设置数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setAll(Request $request): Response
    {
        $result = Model::setAll($this->getData($request));

        return $this->success($result);
    }

    /**
     * 删除一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));

        return $this->success($result);
    }
}
