<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : UploadLog
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-07-11 05:05:27 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\UploadLog as Model;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class UploadLog extends HobooController
{

    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));

        return $this->success($result);
    }

    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItem(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));

        return $this->success($result);
    }
}
