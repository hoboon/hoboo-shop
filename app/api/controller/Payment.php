<?php


namespace app\api\controller;


use app\api\model\UserBill;
use app\service\StorePinkService;
use hoboo\controller\HobooController;
use support\Log;
use support\Request;
use Yansongda\Pay\Pay;

class Payment extends HobooController
{

    public function weixinNotify(Request $request)
    {
        $config=config("payment");
        // 2. 初始化配置
        Pay::config($config);

        $result=json_decode(Pay::wechat()->callback($request->post()),true);
        $ciphertext=$result["resource"]["ciphertext"];
        $transaction_id=$ciphertext["transaction_id"];
        $order_no=$ciphertext["out_trade_no"];
        //查询是否有此订单
        $storeOrderResult=\app\api\model\StoreOrder::getItem(["order_id"=>$order_no]);
        $storeOrderData=$storeOrderResult["data"];
        if(empty($storeOrderData)){
            return $this->fail("此订单为空");
        }

        //将微信支付单号更新至订单表
        $orderResult=\app\api\model\StoreOrder::setItem(
            [
                "id"=>$storeOrderData["id"],
                "trade_no"=>$transaction_id,
                "paid"=>1,
                "pay_time"=>date("Y-m-d H:i:s",time())
            ]
        );
        //如果为拼团订单，则创建拼团记录
        if($storeOrderData["combination_id"]>0){
            $storePinkService=new StorePinkService();
            $pinkResult=$storePinkService->createPink($storeOrderData);
        }
        $userResult=\app\api\model\User::getItem(["id"=>$storeOrderData["uid"] ]);
        $userData=$userResult["data"];
        //记录购买商品消费记录
        $bill_data=[
            'title' => '购买商品',
            'link_id' => $order_no,
            "pm"=>0,
            "uid"=>$userData["id"],
            "category"=>"now_money",
            "type"=>"pay_money",
            "number"=>$storeOrderData["pay_price"],
            "balance"=>$userData["now_money"],
            "mark"=>"支付{$storeOrderData['pay_price']}元购买商品"
        ];
        $billResult=UserBill::setItem($bill_data);
//        Log::info('『微信』回调数据：'.$result);
        return $this->success($orderResult);
    }
    public function weixinReturn(Request $request)
    {
        Log::info('『微信』同步通知'.json_encode($request->get()));
        return 'success';
    }

}