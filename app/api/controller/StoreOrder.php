<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : StoreOrder
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-18 03:02:17 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\StoreOrder as Model;
use app\api\model\UserBill;
use app\service\PaymentService;
use app\service\StoreCartServices;
use app\service\StorePinkService;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Db;
use support\Request;
use support\Response;
use app\api\model\StoreCart;
use app\api\model\StoreOrderCartInfo;
use app\api\model\UserAddress;

class StoreOrder extends HobooController
{


    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {

        $result = Model::getList($this->getData($request));
        //获取订单数据后，对数据进行处理
        $data=$result["data"];
        foreach ($data as &$item){
            $item=$this->tidyOrder($item,true);


            //判断一些参数
        }
        $result["data"]=$data;
        return $this->success($result);
    }
    public function tidyOrder($order,$detail=false)
    {
        if($detail==true && isset($order["id"])){
            $storeOrderCartInfo=new StoreOrderCartInfo();
            $cartInfo=$storeOrderCartInfo->where("oid",$order["id"])->get();
            $info=[];
            //获取订单评论
            foreach ($cartInfo as $k=>$cart){
                $cart_info=json_decode($cart["cart_info"],true);
                $cart_info["unique"]=$k;

                if(isset($cart["productInfo"]["attrInfo"])){

                }
                $cart_info["is_valid"]=1;
                $cart_info["cart_num"]=$cart["cart_num"];
                array_push($info,$cart_info);
                unset($cart);
            }
            $order["cartInfo"]=$info;
        }

        //订单支付状态判断-暂时不做



        return $order;


    }

    public function getDeleteList(Request $request):Response
    {
        $result = Model::getDeleteList($this->getData($request));

        return $this->success($result);
    }

    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));
        $orderData=$result["data"];

        //获取购物车
        $cart_ids=json_decode($orderData["cart_id"],true);
        $cartInfo=[];
        foreach ($cart_ids as $v){
            $cartData=StoreOrderCartInfo::where("cart_id",$v)->sole();
            $cartInfo[]=json_decode($cartData["cart_info"]);
        }

        $orderData["cartInfo"]=$cartInfo;
        $result["data"]=$orderData;
        return $this->success($result);
    }
    public function checkRefund(Request $request):Response
    {
        //2022-7-5 优化执行退款、退款退货的方法
        $params=$this->getData($request);
        //查询订单信息与订单相应商品
        if(!isset($params["id"])){
            return $this->fail("参数不正确");
        }
        try {
            Db::beginTransaction();
            $orderResult=Model::getItem(["id"=>$params["id"]]);
            $orderData=$orderResult["data"];
            //查询订单对应的商品信息
            $cartResult=StoreOrderCartInfo::getList(["oid"=>$params["id"]]);
            $cartData=$cartResult["data"];

            foreach ($cartData as $k=>$v){
                //查询对应的产品，增加库存，减少销量
                $productResult=\app\api\model\StoreProduct::getItem([
                    "id"=>$v["product_id"],
                ]);
                $productData=$productResult["data"];
                //更新库存与销量
                $productData["stock"]+=$v["cart_num"];
                $productData["sales"]-=$v["cart_num"];
                $productResult=\app\api\model\StoreProduct::setItem($productData);
                $productData=$productResult["data"];
            }


            //执行退款申请，修改订单为申请退款
            $result = Model::setItem($params);

            Db::commit();
        }catch (BadRequestHttpException $e){
            $msg = $e->errorMessage;
            Db::rollBack();
            return $this->fail($msg);
        }

        return $this->success($result);
    }
    public function setRefund(Request $request):Response
    {
        $params=$this->getData($request);
        $params["refund_reason_time"]=date("Y-m-d H:i:s",time());
        $result= Model::setItem($params);
        return $this->success($result);
    }
    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
//        $result = Model::setItem($this->getData($request));
        $params=$this->getData($request);
        if(isset($params["is_backend"])){
            unset($params["is_backend"]);
            unset($params["cartInfo"]);

            $result = Model::setItem($params);

            return $this->success($result);
        }
        //支付方式
        $payType = strtolower($params["pay_type"]);
        $shipping_type=1;//暂时只支持物流
        if ($shipping_type==1){
            if(!$params["addressId"]){
                return $this->fail("请选择收获地址");
            }
            $addressInfo=UserAddress::where(["id"=>$params["addressId"]])->sole();
            if(empty($addressInfo)){
                return $this->fail("地址选择有误");
            }
        }else{
            $addressInfo=[];
        }

        try {
            Db::beginTransaction();
            $msg='';
            $couponId=0;
            $seckill_id=0;//秒杀Id
            $combination_id=0;//拼团ID
            $product_id=0;//产品id
            $cart_ids='';//购物车id集合
            $new=1;//是否是立即购买
            $is_create=0;//是否开团
            $is_join=0;//是否参团

            if(isset($params["coupon_id"])){
                $couponId=$params["coupon_id"];//优惠券id
            }

            if(isset($params["product_id"])){
                $product_id=$params["product_id"];
            }
            if(isset($params["cart_id"])){
                $cart_ids=$params["cart_id"];
            }
            if(isset($params["new"])){
                $new=$params["new"];
            }
            if(isset($params["is_create"])){
                $is_create=$params["is_create"];
            }
            if(isset($params["is_join"])){
                $is_join=$params["is_join"];
            }
            $storeCartService=new StoreCartServices();
            $cartInfo=$storeCartService->getUserProductCartList($params["uid"],$cart_ids,$new,$product_id);

            $order=$this->createOrder($params["uid"],$cartInfo,$addressInfo,$payType,$couponId,$seckill_id,$combination_id,$is_create,$is_join);
            DB::commit();
        }catch (BadRequestHttpException $e){

            $msg = $e->errorMessage;
            Db::rollBack();
            return $this->fail($msg);
        }

        $orderId=$order["uuid"];
        if($orderId){
            $result=Model::getItem(["uuid"=>$orderId]);
        }
        if($payType=="wechat"){
            $result["data"]["prepay_weixin"]=$order["prepay_weixin"];
        }

        //立即购买：1，购物车购买：0
        if($params["new"]==0){

            foreach ($cartInfo as $k=>$v){
                StoreCart::delItem(["uuid"=>$v["uuid"]]);
            }

        }

        return $this->success($result);
    }

    /**
     * @param $uid
     * @param $cartInfo
     * @param $addressInfo
     * @param $payType
     * @param int $couponId
     * @param int $seckill_id  秒杀id
     * @return mixed|Response
     * @throws BadRequestHttpException
     */
    public function createOrder($uid,$cartInfo,$addressInfo,$payType,$couponId,$seckill_id,$combination_id,$is_create,$is_join)
    {
        $cartIds=[];
        $totalNum=0;
        $totalPrice=0;
        $payPrice=0;
        //上一个方法获取购物车数据
        foreach ($cartInfo as $k=>$cart){
            $cart["productInfo"]=\app\api\model\StoreProduct::where(["id"=>$cart["product_id"]])->sole();
            $cartIds[]=$cart["id"];
            $totalNum+=$cart["cart_num"];
            $totalPrice+=($cart["productInfo"]["price"]*$cart["cart_num"]);
            $payPrice=$totalPrice;
            //根据购买的商品减去对应的库存
            $productData=$cart["productInfo"];
            if($productData["stock"]==0 || ($productData["stock"]-$cart["cart_num"])<0 ){
                throw new BadRequestHttpException("库存不足!");
            }
            $productData["sales"]+=$cart["cart_num"];
            $productData["stock"]-=$cart["cart_num"];
            $productResult=\app\api\model\StoreProduct::setItem([
                "id"=>$cart["product_id"],
                "sales"=>$productData["sales"],
                "stock"=>$productData["stock"]
            ]);
        }
        //实际支付价=总价-优惠券抵扣价
        if($couponId>0){
            $where=[
                "uid"=>$uid,
                "cid"=>$couponId
            ];
            $couponUser=\app\api\model\StoreCouponUser::getItem($where);
            $couponUserData=$couponUser["data"];
            if($totalPrice<$couponUserData["use_min_price"]){
                throw new BadRequestHttpException("不满足优惠劵的使用条件!");
            }
            $payPrice=$totalPrice-$couponUserData["coupon_price"];
            //将优惠券状态改为已使用
            $couponUserData["state"]=1;
            $couponResult=\app\api\model\StoreCouponUser::setItem($couponUserData->toArray());
        }
        $orderInfo=[
            'uid' => $uid,
            'order_id' => "wx".time(),
            'real_name' => $addressInfo['real_name'],
            'user_phone' => $addressInfo['phone'],
            'user_address' => $addressInfo['province'] . ' ' . $addressInfo['city'] . ' ' . $addressInfo['district'] . ' ' . $addressInfo['detail'],
            'cart_id' => json_encode($cartIds),
            'total_num' => $totalNum,
            'total_price' => $totalPrice,
            'total_postage' => 0,
            'coupon_id' => 0,
            'coupon_price' => 0,
            'pay_price' => $payPrice,
            'pay_postage' => 0,
            'deduction_price' => 0,
//            'paid' => 1,//默认支付完成，之后要改
            'pay_type' => $payType,
            'use_integral' => 0,
            'gain_integral' => 0,
            'mark' => '',
            'combination_id' => $combination_id,
            'pink_id' => 0,
            'seckill_id' => $seckill_id,
            'bargain_id' => 0,
            'cost' => 0,
            'is_channel' => 0,
            'shipping_type' => 1,
            'channel_type' => 'app',
            'province' => '',
            'discount_id' => 0,
            'spread_uid' => 0,
            'spread_two_uid' => 0,
            "state"=>0
        ];
        //保存订单数据
        $result=Model::setItem($orderInfo);
        $order=$result["data"];
        if(empty($order)){
            return $this->fail("订单生成失败");
        }

        //保存购物车商品信息
        $storeOrderCart=new StoreOrderCartInfo();
        $storeOrderCart->setCartInfo($order["id"],$cartInfo);

        $userResult=\app\api\model\User::getItem(["id"=>$uid]);
        $userData=$userResult["data"];
        //用户扣款
        if($payType=="yue"){

            $now_money=$userData["now_money"]-$payPrice;
            if($now_money<0){
                throw new BadRequestHttpException("用户余额不足");
            }
            $result=\app\api\model\User::setItem([
                "id"=>$uid,
                "now_money"=>$now_money
            ]);
            //2022-6-21日，扣款成功后，将订单支付状态改为1
            $updateOrder=Model::setItem(["id"=>$order["id"],"paid"=>1,"pay_time"=>date("Y-m-d H:i:s",time())]);
            $order=$updateOrder["data"];
            if($combination_id>0){
                //进行拼团操作
                $orderResult=Model::getItem(["id"=>$order["id"]]);
                $orderData=$orderResult["data"];
                $storePinkService=new StorePinkService();
                $pinkResult=$storePinkService->createPink($orderData,$is_create,$is_join);
                $pinkData=$pinkResult["data"];
                //更新订单的pinkid
                $updateResult=Model::setItem([
                    "id"=>$orderData["id"],
                    "pink_id"=>$pinkData["id"]
                ]);
            }
            //记录购买商品与余额变动记录
            //余额变动
            $bill_data=[
                'title' => '余额支付购买商品',
                'link_id' => $order["id"],
                "pm"=>0,
                "uid"=>$userData["id"],
                "category"=>"now_money",
                "type"=>"pay_product",
                "number"=>$payPrice,
                "balance"=>$userData["now_money"],
                "mark"=>"余额支付{$payPrice}元购买商品"
            ];
            $billResult=UserBill::setItem($bill_data);
            //购买商品
            $bill_data=[
                'title' => '购买商品',
                'link_id' => $order["id"],
                "pm"=>0,
                "uid"=>$userData["id"],
                "category"=>"now_money",
                "type"=>"pay_money",
                "number"=>$payPrice,
                "balance"=>$now_money,
                "mark"=>"支付{$payPrice}元购买商品"
            ];
            $billResult=UserBill::setItem($bill_data);
        }
        //用户微信支付
        if($payType=="wechat"){
            //todo:微信支付处理流程
            $paymentService=new PaymentService();
            //获取当前用户绑定的微信Openid
            $wechatUserResult=\app\api\model\WechatUser::getItem(["uid"=>$uid]);
            $wechatUserData=$wechatUserResult["data"];
            if(empty($wechatUserData)){
                throw new BadRequestHttpException("当前用户未绑定微信账户，无法进行支付");
            }
            $preWeixinOrder=$paymentService->getMiniWexinPrePay($order,$userData,$wechatUserData);
//            \support\Cache::set($uid."_wxorder_".$order["id"],$preWeixinOrder);
            $order["prepay_weixin"]=$preWeixinOrder;
        }

        //

        return $order;

    }

    /**
     * 拼团 取消开团
     * @param Request $request
     * @return Response
     */
    public function remove(Request $request):Response
    {
        $params=$this->getData($request);
        if(!isset($params["id"] )|| !isset($params["cid"]) || !isset($params["uid"])){
            return $this->fail("缺少参数");
        }
        $pinkService=new StorePinkService();
        $orderResult=$pinkService->removePink($params["uid"],$params["cid"],$params["id"]);
        return $this->success($orderResult);
    }

    /**
     * 批量创建或更新数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {
        $result = Model::setItems($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量设置数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setAll(Request $request): Response
    {
        $result = Model::setAll($this->getData($request));

        return $this->success($result);
    }

    /**
     * 删除一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));

        return $this->success($result);
    }

    /**
     * @param Request $request
     * @return Response
     * 获取客户要重新支付的未支付订单
     */
    public function getUnPayOrder(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));
        $order=$result["data"];
        //对订单数据进行数据格式化

    }

    /**
     * @param Request $request
     * @return Response
     * 设置重新支付未支付订单操作方法
     */
    public function setUnPayOrder(Request $request): Response
    {
        $params=$this->getData($request);
        //支付方式
        $payType = strtolower($params["pay_type"]);
        $shipping_type=1;//暂时只支持物流
        if ($shipping_type==1){
            if(!$params["addressId"]){
                return $this->fail("请选择收获地址");
            }
            $addressInfo=UserAddress::where(["id"=>$params["addressId"]])->sole();
            if(empty($addressInfo)){
                return $this->fail("地址选择有误");
            }
        }else{
            $addressInfo=[];
        }
        $orderInfoResult=Model::getItem(["id"=>$params["id"]]);
        $orderInfoData=$orderInfoResult["data"];
        $orderInfoData["pay_type"]=$payType;
        $orderInfoData["real_name"]=$addressInfo['real_name'];
        $orderInfoData["user_phone"]=$addressInfo['phone'];
        $orderInfoData["user_address"]=$addressInfo['province'] . ' ' . $addressInfo['city'] . ' ' . $addressInfo['district'] . ' ' . $addressInfo['detail'];
        try {
            Db::beginTransaction();
            $msg='';
            $couponId=$params["coupon_id"];
            $payPrice=$orderInfoData["pay_price"];
            //实际支付价=总价-优惠券抵扣价
            if($couponId>0){
                $where=[
                    "uid"=>$orderInfoData["uid"],
                    "cid"=>$couponId
                ];
                $couponUser=\app\api\model\StoreCouponUser::getItem($where);
                $couponUserData=$couponUser["data"];
                if($orderInfoData["total_price"]<$couponUserData["use_min_price"]){
                    throw new BadRequestHttpException("不满足优惠劵的使用条件!");
                }
                $payPrice=$orderInfoData["total_price"]-$couponUserData["coupon_price"];
                $orderInfoData["pay_price"]=$payPrice;
                //将优惠券状态改为已使用
                $couponUserData["state"]=1;
                $couponResult=\app\api\model\StoreCouponUser::setItem($couponUserData->toArray());
            }

            //更新数据
            $userResult=\app\api\model\User::getItem(["id"=>$orderInfoData["uid"]]);
            $userData=$userResult["data"];
            //用户扣款

            if($payType=="yue"){

                $now_money=$userData["now_money"]-$payPrice;
                if($now_money<0){
                    throw new BadRequestHttpException("用户余额不足");
                }
                $result=\app\api\model\User::setItem([
                    "id"=>$orderInfoData["uid"],
                    "now_money"=>$now_money
                ]);
                //2022-6-21日，扣款成功后，将订单支付状态改为1
                $orderInfoData["paid"]=1;
                $orderInfoData["pay_time"]=date("Y-m-d H:i:s",time());
                $updateOrder=Model::setItem($orderInfoData->toArray());

                $order=$updateOrder["data"];
                //记录购买商品与余额变动记录
                //余额变动
                $bill_data=[
                    'title' => '余额支付购买商品',
                    'link_id' => $order["id"],
                    "pm"=>0,
                    "uid"=>$userData["id"],
                    "category"=>"now_money",
                    "type"=>"pay_product",
                    "number"=>$payPrice,
                    "balance"=>$userData["now_money"],
                    "mark"=>"余额支付{$payPrice}元购买商品"
                ];
                $billResult=UserBill::setItem($bill_data);
                //购买商品
                $bill_data=[
                    'title' => '购买商品',
                    'link_id' => $order["id"],
                    "pm"=>0,
                    "uid"=>$userData["id"],
                    "category"=>"now_money",
                    "type"=>"pay_money",
                    "number"=>$payPrice,
                    "balance"=>$now_money,
                    "mark"=>"支付{$payPrice}元购买商品"
                ];
                $billResult=UserBill::setItem($bill_data);
            }
            //用户微信支付
            if($payType=="wechat"){
                //todo:微信支付处理流程
                $paymentService=new PaymentService();
                //先更新订单数据，paid数据之后更新
                $updateOrder=Model::setItem($orderInfoData->toArray());
                $order=$updateOrder["data"];
                //获取当前用户绑定的微信Openid
                $wechatUserResult=\app\api\model\WechatUser::getItem(["uid"=>$order["uid"]]);
                $wechatUserData=$wechatUserResult["data"];
                if(empty($wechatUserData)){
                    throw new BadRequestHttpException("当前用户未绑定微信账户，无法进行支付");
                }
                $preWeixinOrder=$paymentService->getMiniWexinPrePay($order,$userData,$wechatUserData);
//            \support\Cache::set($uid."_wxorder_".$order["id"],$preWeixinOrder);
                $order["prepay_weixin"]=$preWeixinOrder;
                //更新ORDER数据
                $updateOrder["data"]=$order;
            }

            DB::commit();
        }catch (BadRequestHttpException $e){
            $msg = $e->errorMessage;
            Db::rollBack();
            return $this->fail($msg);
        }
        return $this->success($updateOrder);
    }

    /**
     * @param Request $request
     * @return Response
     * 拒绝退款
     */
    public function noRefund(Request $request):Response
    {
        $params=$this->getData($request);

        

        if (isset($params["cartInfo"])){
            unset($params["cartInfo"]);
        }

        //拒绝退款，将退款状态重置为正常
        if($params["refund_type"]==2){
            $params["state"]=1;
        }
        if(empty($params["delivery_name"])){
            $params["state"]=0;
        }
        $params["refund_type"]=0;
        $params["refund_status"]=0;

        $result=Model::setItem($params);
        return $this->success($result);
    }

    /**
     * @param Request $request
     * @return Response
     * 后台同意且立即退款
     */
    public function allowRefund(Request $request):Response
    {
        $params=$this->getData($request);
        try {
            Db::beginTransaction();
            if (isset($params["cartInfo"])){
                unset($params["cartInfo"]);
            }

            //同意退款，将订单状态改为已退款或已退款退货
            if($params["refund_type"]==2){
                $params["state"]=-2;
            }
            //
            $params["refund_status"]=2;

            $result=Model::setItem($params);

            //将退款的金额返还至用户余额
            //todo:根据支付方式，退款到对应的账户
//        if($params["pay_type"]=="yue"){
//
//        }
            $userResult=\app\api\model\User::getItem(["id"=>$params["uid"]]);
            $userInfo=$userResult["data"];
            $userInfo["now_money"]+=$params["refund_price"];

            //记录入user_bill表
            $add_data=[
                "uid"=>$userInfo["id"],
                "link_id"=>$params["id"],
                "pm"=>1,
                "title"=>"商品退款",
                "category"=>"now_money",
                "type"=>"pay_product_refund",
                "number"=>$params["refund_price"],
                "balance"=>$userInfo["now_money"],
                "mark"=>"订单退款余额支付{$params["refund_price"]}元",
            ];
            $result=UserBill::setItem($add_data);
            $result=\app\api\model\User::setItem(["id"=>$userInfo["id"],"now_money"=>$userInfo["now_money"]]);
            Db::commit();
        }catch (BadRequestHttpException $e){
            $msg = $e->errorMessage;
            Db::rollBack();
            return $this->fail($msg);
        }
        return $this->success($result);

    }



}
