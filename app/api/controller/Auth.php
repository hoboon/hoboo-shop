<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : Auth
 * @Author          : mr.box
 * @Createtime      : 2022/3/8 4:47 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\SystemMenus as SystemMenuModel;
use app\api\model\SystemAdmin;
use app\api\model\SystemLog as SystemLog;
use Gregwar\Captcha\CaptchaBuilder;
use Gregwar\Captcha\PhraseBuilder;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\serve\Account;
use hoboo\serve\SmsServe;
use Illuminate\Support\Facades\Date;
use support\Request;
use support\Response;
use Tinywan\Jwt\JwtToken;

class Auth extends HobooController
{
    /**
     * 获取一个短信验证码
     * @param $request
     * @param $data
     * @return Response
     * @throws BadRequestHttpException
     */
    public function sendSms($request, $data): Response
    {
        
        if (!isset($data['phone_number'])) {
            throw new BadRequestHttpException('请输入手机号码');
        }
        
        /**
         * 验证码生成
         */
        // 初始化验证码类
        $builder = new CaptchaBuilder(null, new PhraseBuilder(6, '0123456789'));
        // 生成验证码
        $data['code'] = $builder->getPhrase();
        
        // 将验证码的值存储到Cache中，获取验证码键值
        $captcha_key = (new Account($request))->createLoginKey($data);
        
        // 发送短信
        $sendStatus = (new SmsServe(['type' => 'aliyun']))->sendSms($data);
        
        $result['data'] = [
            'code'        => $data['code'],
            'key'         => $captcha_key,
            'send_status' => $sendStatus
        ];
        
        return $this->success($result);
    }
    
    /**
     * 验证码统一入口
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function captcha(Request $request): Response
    {
        $data = $this->getData($request);
        if (isset($data['phone_number'])) {
            return $this->sendSms($request, $data);
        } else {
            return $this->sendCaptcha($request);
        }
    }
    
    /**
     * 获取一个验证码
     * @param  Request  $request
     * @return Response
     */
    public function sendCaptcha(Request $request): Response
    {
        /**
         * 请求参数预处理
         *
         * 访问客户端判断....
         * $data = $this->getData($request);
         * $this->app_client = $data['client']; // 主动上报
         */
        
        /**
         * 验证码生成
         */
        // 初始化验证码类
        $builder = new CaptchaBuilder(null, new PhraseBuilder(4));
        // 生成验证码
        $captcha_code = $builder->getPhrase();
        // 将验证码的值存储到Cache中，获取验证码键值
        $captcha_key = (new Account($request))->createLoginKey(['code' => $captcha_code]);
        
        // 获取验证码图片二进制数据
        $result['captcha'] = $builder->build()->inline();
        
        /**
         * 设置响应头中的验证码键值（用于验证码校验）
         */
        $header = ['Captcha-Key' => $captcha_key];
        
        /**
         * 测试用，生产环境需删除
         */
        $result['code'] = $captcha_code;
        $result['key'] = $captcha_key;
        
        $_result['data'] = $result;
        
        return $this->success($_result, $header);
    }
    
    /**
     * @desc 账户登陆
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function login(Request $request): Response
    {
        $data = $this->getData($request);
        
        // 校验：验证码校验码
        if (!isset($data['key']) && !isset($data['phone_number'])) {
            throw new BadRequestHttpException('无效的校验码');
        }
        
        // 验证码，验证
        (new Account($request))->checkCode($data);
        
        if (empty($data['password'])) {
            throw new BadRequestHttpException('请输入密码');
        }
        
        // 账户验证
        if (!isset($data['account'])) {
            throw new BadRequestHttpException('请输入账号');
        } else {
            // 用户基本信息
            $account = $this->getAccount($data);
            
            // 登陆日志
            $this->setLoginLog($request, array_merge($data, is_null($account) ? [] : $account->toArray()));
            
            // 账户存在性判断
            if (empty($account)) {
                throw new BadRequestHttpException('账户不存在');
            } else {
                // 密码验证
                if (password_verify((string) $data['password'], $account['password'])) {
                    unset($account['password']);
                    // 生成Token
                    $token = JwtToken::generateToken($account->toArray());
                    
                    // 获取用户权限菜单
                    $items = SystemMenuModel::getAuthList(['id' => explode(',', $account['roles'])]);
                    /**
                     * header token
                     */
                    $header = [
                        'Token-Type'    => $token['token_type'],
                        'Expires-In'    => $token['expires_in'],
                        'Access-Token'  => $token['access_token'],
                        'Refresh-Token' => $token['refresh_token'],
                    ];
                    
                    $result['message'] = '登陆成功';
                    $result['data']['items'] = $items['data'];
                    $result['data']['user'] = $account;
                } else {
                    throw new BadRequestHttpException('密码错误');
                }
            }
        }
        
        return $this->success($result, $header);
    }
    
    /**
     * @param  array  $data
     * @return mixed
     */
    private function getAccount(array $data): mixed
    {
        return match ($data['type']) {
            'admin_login' => SystemAdmin::getUserByAccount($data),
            default => \app\api\model\OrganizationUser::getUserByAccount($data)
        };
    }
    
    /**
     * 管理员登陆日志
     *
     * @param  Request  $request
     * @param  array  $data
     * @return void
     * @throws BadRequestHttpException
     */
    private function setLoginLog(Request $request, array $data): void
    {
        // 获取客户端真实IP
        $ip = $request->getRealIp();
        
        $type = $data['type'] === 'admin_login' ? 1 : 0;
        
        /**
         * 日志记录
         */
        $log_data = [
            'admin_id'   => $data['id'] ?? 0,
            'admin_name' => $data['account'],
            'path'       => $request->url(),
            'page'       => $request->fullUrl(),
            'method'     => $request->method(),
            'ip'         => $ip,
            'type'       => $type,
        ];
        
        SystemLog::setItem($log_data);
        
        if (empty($data['id'])) {
            throw new BadRequestHttpException('账户不存在');
        }
        
        /**
         * 最后登陆记录
         */
        $log_data = [
            'id'          => $data['id'],
            'last_ip'     => $ip,
            'last_time'   => Date::now(),
            'login_count' => $data['login_count'] + 1,
        ];
        
        match ($data['type']) {
            'admin_login' => SystemAdmin::setLastLogin($log_data),
            default => \app\api\model\OrganizationUser::setLastLogin($log_data)
        };
    }
}
