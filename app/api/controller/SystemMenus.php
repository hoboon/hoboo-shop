<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : SystemMenus
 * @Author          : mr.box
 * @Createtime      : 2022/3/3 2:19 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\SystemMenus as Model;
use Exception;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class SystemMenus extends HobooController
{
    /**
     * 批量新增
     *
     * @date 2022.04.06
     * @param  Request  $request
     * @return Response
     * @throws Exception
     */
    public function initialization(Request $request): Response
    {
        $result = Model::initialization($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 获取数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 创建或更新一个菜单信息
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 删除一个数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 获取一个菜单配置
     * @param  Request  $request
     * @return Response
     */
    public function getMenuList(Request $request): Response
    {
        $result = Model::getMenuList($this->getData($request));
        
        return $this->success($result);
    }
}
