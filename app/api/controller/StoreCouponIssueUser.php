<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : StoreCouponIssueUser
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-18 03:02:17 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\StoreCouponIssueUser as Model;
use app\api\model\StoreCouponIssue;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class StoreCouponIssueUser extends HobooController
{

    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $params=$this->getData($request);
        $result = Model::getList($params);

        $data=$result["data"];
        $storeCouponIssue=StoreCouponIssue::getList([]);
        $couponIssueData=$storeCouponIssue["data"];
        if(empty($data)){
            foreach ($couponIssueData as &$item){
                $item["is_owned"]=0;
            }
        }else{
            foreach ($couponIssueData as $k=>$v){

                $couponIssueData[$k]["is_owned"]=0;
                $couponUserResult=Model::getItem(["uid"=>$params["uid"],"issue_coupon_id"=>$v["id"]]);
                $couponUserData=$couponUserResult["data"];
//                dd($couponUserData["issue_coupon_id"]);
                if(!empty($couponUserData)){
                    $couponIssueData[$k]["is_owned"]=1;
                }
            }
        }
        $result["data"]=$data;
        $result["store_coupon_issue"]=$couponIssueData;
//        dd($couponIssueData);
        return $this->success($result);
    }

    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量创建或更新数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {
        $result = Model::setItems($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量设置数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setAll(Request $request): Response
    {
        $result = Model::setAll($this->getData($request));

        return $this->success($result);
    }

    /**
     * 删除一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));

        return $this->success($result);
    }
}
