<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : UserGroup
 * @Author          : mr.box
 * @Createtime      : 2022/3/3 2:19 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\UserGroup as Model;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class UserGroup extends HobooController
{
    
    
    /**
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 创建或更新一个用户组
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 「批量」创建或更新一个用户组
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {
        $result = Model::setItems($this->getData($request));
        
        return $this->success($result);
    }
    
    /**
     * 删除一个用户组
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $data = $request->post();
        
        $result = Model::delItem($data);
        
        return $this->success($result);
    }
    
    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));
        
        return $this->success($result);
    }
}
