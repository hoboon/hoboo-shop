<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : StoreCart
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-18 03:02:18 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\StoreCart as Model;
use app\api\model\StoreProduct;
use app\api\model\User;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class StoreCart extends HobooController
{

    public function setCart(Request $request):Response
    {
        $result=Model::setCart($this->getData($request));
        return $this->success($result);
    }

    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $params=$this->getData($request);
        if(isset($params["is_order_confirm"])){
            $params["id"]=explode(",",rtrim($params["id"],","));
            unset($params["is_order_confirm"]);
        }
        $result = Model::getList($params);
        //获取到用户购物车数据后对其进行数据处理
        $data=$result["data"];

        $storeProduct=new StoreProduct();

        $userModel=new User();

        foreach ($data as &$item){
            $productInfo=$storeProduct::where("id",$item["product_id"])->sole();
            $item["productInfo"]=$productInfo;
            //判断商品规格与属性，暂时留空
            if(isset($productInfo['attrInfo']['product_id']) ){

            }else{
                $item['costPrice'] = $item['productInfo']['cost'] ?? 0;
                $item['trueStock'] = $item['productInfo']['stock'] ?? 0;
                $item["truePrice"]=$item["productInfo"]["price"]??0;
//                $item['truePrice'] = $productServices->setLevelPrice($item['productInfo']['price'] ?? 0, $uid, $userInfo, $vipStatus, true, $discount, $item['attrInfo']['vip_price'] ?? 0, $item['productInfo']['is_vip'] ?? 0);
//                $item['vip_truePrice'] = (float)$productServices->setLevelPrice($item['productInfo']['price'] ?? 0, $uid, $userInfo, $vipStatus, false, $discount, $item['attrInfo']['vip_price'] ?? 0, $item['productInfo']['is_vip'] ?? 0);

            }
        }

        $result["data"]=$data;
        return $this->success($result);
    }



    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量创建或更新数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {

        $result = Model::setItems($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量设置数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setAll(Request $request): Response
    {
        $result = Model::setAll($this->getData($request));

        return $this->success($result);
    }

    /**
     * 删除一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));

        return $this->success($result);
    }
}
