<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : MemberCard
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-18 03:02:12 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\MemberCard as Model;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class MemberCard extends HobooController
{

    /**计算会员天数
     * @param Request $request
     * @return mixed
     */
    public function getOverdueTime(Request $request):Response
    {
        $params=$this->getData($request);
        $member_type = $params['member_type'];
        $vip_day = $params['vip_day'];
        //获取用户信息
        $user_info=\app\api\model\User::where("id",$params["uid"])->sole();
        if ($member_type == 'ever') {
            $overdue_time = 0;
            $is_ever_level = 1;
        } else {
            if ($user_info['is_money_level'] == 0) {
                $overdue_time = bcadd(bcmul($vip_day, 86400, 0), time(), 0);
            } else {
                $overdue_time = bcadd(bcmul($vip_day, 86400, 0), $user_info['overdue_time'], 0);
            }
            $is_ever_level = 0;
        }
        if ($is_ever_level == 1 || $user_info['is_ever_level']) {
            $res = "永久会员";
        } else {
            $res = date('Y-m-d', $overdue_time);
        }
        $result["data"]=$res;
        return $this->success($result);
    }

    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));

        return $this->success($result);
    }

    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量创建或更新数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {
        $result = Model::setItems($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量设置数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setAll(Request $request): Response
    {
        $result = Model::setAll($this->getData($request));

        return $this->success($result);
    }

    /**
     * 删除一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));

        return $this->success($result);
    }
}
