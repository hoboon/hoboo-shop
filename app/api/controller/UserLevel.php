<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : UserLevel
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-24 02:59:54 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\UserLevel as Model;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class UserLevel extends HobooController
{

    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));

        return $this->success($result);
    }

    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));
    
        /**
         * TODO 根据当前用户等级获取任务参数
         */
        $result['data']['taskInfo'] = [
            'exp_sign'   => 1,  // 用户当前会员等级，【签到】获取的经验值
            'sign_count' => 1,  // 用户总签到次数
            'exp_order'  => 1,  // 用户当前会员等级，【下单】获取的经验值
            'exp_invite' => 1,  // 用户当前会员等级，【邀请】获取的经验值
        ];

        return $this->success($result);
    }

    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $result = Model::setItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量创建或更新数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {
        $result = Model::setItems($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量设置数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setAll(Request $request): Response
    {
        $result = Model::setAll($this->getData($request));

        return $this->success($result);
    }

    /**
     * 删除一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));

        return $this->success($result);
    }
}
