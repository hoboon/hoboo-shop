<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : User
 * @Author          : mr.box
 * @Createtime      : 2022/3/3 2:19 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\User as Model;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use hoboo\serve\Account;
use support\Request;
use support\Response;
use Tinywan\Jwt\JwtToken;
use app\api\model\UserRecharge;
use app\api\model\UserBill;
use support\Cache;

class User extends HobooController
{

    /**
     * @param Request $request
     * @return Response
     * 获取分销员列表
     */
    public function agentUser(Request $request):Response
    {
        $result=Model::getList($this->getData($request));

        $userData=$result["data"];

        //组合页面上需要的数据
        foreach ($userData as &$user){
            //当前用户推广的下级人员
            $res=Model::getList(["spread_uid"=>$user["id"]]);
            $resData=$res["data"];
            $spread_count=count($resData);
            //todo:之后每次有下级会员注册，spread_count数值+1
            $user["spread_count"]=$spread_count;

            //订单数量
            //todo:目前只做一级的推广订单统计
            $order_res=\app\api\model\StoreOrder::getList(["spread_uid"=>$user["id"]]);
            $order_count=count($order_res["data"]);
            $user["order_count"]=$order_count;
            $order_price=0;
            foreach ($order_res["data"] as $k=>$v){
                $order_price+=$v["total_price"];
            }
            //todo:用户分销等级需要获取
            $user["agent_level"]=0;
            //todo:用户佣金
            $user["brokerage_money"]=0.00;
            //todo:已提现金额
            $user["extract_count_price"]=0.00;
            //todo：提现次数
            $user["extract_count_num"]=0.00;
            //todo:未提现金额
            $user["not_extract"]=0.00;
            //todo:上级推广人
            $user["spread_name"]="无";
        }

        $result["data"]=$userData;
        return $this->success($result);

    }

    public function login(Request $request):Response
    {
        $data=$this->getData($request);
        if(isset($data["pwd"])){
            $password=$data["pwd"];
            unset($data["pwd"]);
        }
        $result=Model::getItem($data);
        $user=$result["data"];
        if(empty($user)){
            return $this->fail("登录失败，请确认正确的用户名与密码");
        }
        if (password_verify((string) $password, $user['pwd'])) {
            unset($user["password"]);
            // 生成Token
            $token = JwtToken::generateToken($user->toArray());
            $header = [
                'Token-Type'    => $token['token_type'],
                'Expires-In'    => $token['expires_in'],
                'Access-Token'  => $token['access_token'],
                'Refresh-Token' => $token['refresh_token'],
            ];
        }
        $result["data"]=$user;
        return $this->success($result,$header);
    }
    
    public function register(Request $request):Response
    {
        $params=$this->getData($request);
        //查询用户是否有注册过
        $result=Model::getItem(["phone"=>$params["phone"] ] );
        if(!empty($result["data"])){
            return $this->fail("该用户已注册");
        }
        //查询推广员是否存在，不存在则为0
        if($params["spread_phone"]==""){
            $spread_uid=0;
        }else{
            //查询是否有此用户，是否有推销资格
            $spreadResult=Model::getItem(["phone"=>$params["spread_phone"]]);
            $spread_user=$spreadResult["data"];
            if(empty($spread_user)){
                $spread_uid=0;
            }else{
                $spread_uid=$spread_user["id"];
            }
        }
        $add_data=[
            "account"=>$params["phone"],
            "phone"=>$params["phone"],
            "pwd"=>$params["password"],
            "spread_uid"=>$spread_uid,
            "spread_time"=>time(),
            "nickname"=>substr(md5($params["phone"] . time()), 0, 12)
        ];
        $result=Model::setItem($add_data);

        $result=Model::getItem($result["data"]);
        unset($result["data"]["pwd"]);
        return $this->success($result);
    }
    /**
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));
        
        return $this->success($result);
    }
    public function personalHome(Request $request):Response
    {
        $result = Model::getItem($this->getData($request));
        $userData=$result["data"];
        $uid=$userData["uid"];

    }

    /**
     * @param Request $request
     * @return Response
     * 用户充值操作
     */
    public function setUserRecharge(Request $request):Response
    {
        $params=$this->getData($request);
        if(empty($params["uid"]) || $params["uid"]==0){
            return $this->fail("用户id不能为0");
        }
        if(empty($params["recharge"]) || $params["recharge"]==0 ){
            return $this->fail("充值金额不能为0");
        }
        $result=Model::getItem(["id"=>$params["uid"]]);
        $userData=$result["data"];

        //将充值记录计入user_recharge表
        $recharge_data=[
            "uid"=>$params["uid"],
            "order_id"=>"wx".date("Ymd",time()).time(),
            "price"=>$params["recharge"],
            "give_price"=>0.00,
            "recharge_type"=>"balance",
            "paid"=>1,
            "pay_time"=>time(),
            "state"=>1
        ];
        $rechargeResult=UserRecharge::setItem($recharge_data);
        $rechargeResultData=$rechargeResult["data"];

        //将充值记录计入user_bill表
        $bill_data=[
            'title' => '系统增加用户余额',
            'link_id' => $rechargeResultData["id"],
            "pm"=>1,
            "category"=>"now_money",
            "type"=>"system_add",
            "number"=>$params["recharge"],
            "balance"=>$userData["now_money"],
            "mark"=>"系统增加了{$params['recharge']}余额"
        ];
        $billResult=UserBill::setItem($bill_data);
        $rechargeMoney=$userData["now_money"]+$params["recharge"];
        $updateResult=Model::setItem(["id"=>$params["uid"],"now_money"=>$rechargeMoney]);

        return $this->success($updateResult);

    }
    
    /**
     * @param  Request  $request
     * @return Response
     */
    public function getUserList(Request $request): Response
    {
        $data = array_merge($request->get(), $request->post());
        $result = Model::getUserList($data);
        
        return $this->success($result);
    }
    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $params=$this->getData($request);

        $result = Model::getItem($params);
        return $this->success($result);
    }


        /**
     * 创建或更新一个用户组
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $params=$this->getData($request);
        if(isset($params["is_vip"])){
            unset($params["is_vip"]);
        }
        $result = Model::setItem($params);
        
        return $this->success($result);
    }
    public function bindPhone(Request $request): Response
    {
        $params=$this->getData($request);
        if(empty($params["phone"])){
            return $this->fail("手机号不能为空");
        }
        if($params["captcha"]){
            $data=[
                "code"=>$params["captcha"],
                "key"=>$params["phone"]
            ];
            //判断短信验证码是否正确
            (new Account($request))->checkCode($data);
        }
        unset($params["captcha"]);
        //先查询此号码是否有被绑定
        $phoneResult=Model::getItem(["phone"=>$params["phone"]]);
        $phoneData=$phoneResult["data"];
        if(!empty($phoneData)){
            return $this->fail("该手机号已绑定");
        }
        $update_data=[
            "id"=>$params["id"],
            "phone"=>$params["phone"]
        ];
        $result=Model::setItem($update_data);
        return $this->success($result);
    }
    
    /**
     * 删除一个用户组
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $data = $request->post();
        
        $result = Model::delItem($data);
        
        return $this->success($result);
    }
    
    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));
        
        return $this->success($result);
    }

    public function resetPassword(Request $request): Response
    {
        $params=$this->getData($request);
        $params["pwd"]="123456";

        $result=Model::setItem($params);
        return $this->success($result);
    }

}
