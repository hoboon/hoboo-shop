<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : UploadFile
 * @Author          : mr.box
 * @Createtime      : 2022/6/9 10:42
 * @Description     : ...
 */


namespace app\api\controller;


use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\serve\StorageServe;
use support\Request;
use support\Response;

class UploadFile extends HobooController
{
    /**
     * @desc 上传一个文件
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $data = $this->getData($request);
        
        $result = (new StorageServe())->uploadFile($data);
    
        // 单文件上传 返回数据结构调整为 文件参数对象
        if (count($result['data'])) {
            $result['data'] = $result['data'][0];
        }
        
        return $this->success($result);
    }
    
}
