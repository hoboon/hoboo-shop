<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : UserSign
 * @Author          : Hoboo Tech
 * @Createtime      : 2022-05-18 03:02:13 PM
 * @Description     : ...
 */


namespace app\api\controller;


use app\api\model\UserSign as Model;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use hoboo\exception\EmptyDataHttpException;
use support\Request;
use support\Response;

class UserSign extends HobooController
{

    //获取今日是否签到的
    public function getTodaySign(Request $request): Response
    {
        $params=$this->getData($request);
        $params["today"]=date("Y-m-d",time());
        $result = Model::getItem($params);
        $signData=$result["data"];
        if(empty($signData)){
            $signData["is_day_sign"]=0;
        }else{
            //判断今天有没有签到过
            $today=date("Y-m-d",time());
            $signDay=date("Y-m-d",strtotime($signData["create_time"]));
            if($today==$signDay){
                $signData["is_day_sign"]=1;
            }else{
                $signData["is_day_sign"]=0;
            }
        }
        $result["data"]=$signData;
        return $this->success($result);

    }
    /**
     * 获取一个数据列表
     *
     * @param  Request  $request
     * @return Response
     */
    public function getList(Request $request): Response
    {
        $result = Model::getList($this->getData($request));

        return $this->success($result);
    }

    /**
     * 获取一条数据记录
     *
     * @param  Request  $request
     * @return Response
     */
    public function getItem(Request $request): Response
    {
        $result = Model::getItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 创建或更新一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     */
    public function setItem(Request $request): Response
    {
        $params=$this->getData($request);
        $userResult=\app\api\model\User::getItem(["id"=>$params["uid"]]);
        $userData=$userResult["data"];
        $num=10;
        $userData["integral"]+=$num;
        $userData["exp"]+=$num;
        $add_data=[
            "id"=>$userData["id"],
            "integral"=>$userData["integral"],
            "exp"=>$userData["exp"]
        ];
        $updateResult=\app\api\model\User::setItem($add_data);
        $add_data=[
            "uid"=>$params["uid"],
            "title"=>"签到获取积分",
            "number"=>10,
            "balance"=>$userData["integral"]
        ];
        $result = Model::setItem($add_data);
        //记录成功后，将更改的积分数值记录入user_bill

        $add_data=[
            "uid"=>$userData["id"],
            'title' => '签到奖励',
            'category' => 'integral',
            'type' => 'sign',
            'mark' => '签到奖励赠送'.$num.'积分',
            'state' => 1,
            'pm' => 1
        ];
        $result=\app\api\model\UserBill::setItem($add_data);
        $add_data=[
            "uid"=>$userData["id"],
            'title' => '签到奖励',
            'category' => 'exp',
            'type' => 'sign',
            'mark' => '签到奖励赠送'.$num.'',
            'state' => 1,
            'pm' => 1
        ];
        $result=\app\api\model\UserBill::setItem($add_data);
        return $this->success($result);
    }

    /**
     * 批量创建或更新数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setItems(Request $request): Response
    {
        $result = Model::setItems($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量设置数据
     *
     * @param  Request  $request
     * @return Response
     */
    public function setAll(Request $request): Response
    {
        $result = Model::setAll($this->getData($request));

        return $this->success($result);
    }

    /**
     * 删除一条数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItem(Request $request): Response
    {
        $result = Model::delItem($this->getData($request));

        return $this->success($result);
    }

    /**
     * 批量删除数据
     *
     * @param  Request  $request
     * @return Response
     * @throws BadRequestHttpException
     * @throws EmptyDataHttpException
     */
    public function delItems(Request $request): Response
    {
        $result = Model::delItems($this->getData($request));

        return $this->success($result);
    }
}
