<?php

namespace app\command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ModuleCreateCommand extends Command
{
    protected static $defaultName = 'module:create';
    protected static $defaultDescription = '用于生成[业务控制器、数据模型]，创建默认约定方法';
    
    /**
     * @return void
     */
    protected function configure()
    {
        $this->addArgument('app', InputArgument::REQUIRED, '多应用，子项目名称');
        $this->addArgument('name', InputArgument::REQUIRED, '业务名称，控制器、模型将以该名称命名（首字母自动转为大写）');
    }
    
    /**
     * @param  InputInterface  $input
     * @param  OutputInterface  $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // 应用名
        $app = $input->getArgument('app');
        $app = $app ? 'app/'.$app.'/' : '';
        
        // 文件名
        $name = $input->getArgument('name');
        $_name = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
        
        /**
         * @desc 创建控制器
         * @date 2022-03-17
         */
        $controller_filename = $app.'controller/'.$_name.'.php';
        $controller_temp = 'hoboo/template/TemplateController.temp';
        $controller_content = file_exists($controller_temp) ? file_get_contents($controller_temp) : '';
        if ($controller_content !== '') {
            $controller_content = $this->handleContent($controller_content, $_name, $name);
            $this->createFile($controller_filename, $controller_content);
            
            $output->writeln('[SUCCESS]'.$_name.' 控制器创建成功');
        } else {
            $output->writeln('[WARNING]'.$_name.' 控制器创建失败');
        }
        
        /**
         * @desc 创建模型
         * @date 2022-03-17
         */
        $model_filename = $app.'model/'.$_name.'.php';
        $model_temp = 'hoboo/template/TemplateModel.temp';
        $model_content = file_exists($model_temp) ? file_get_contents($model_temp) : '';
        if ($model_content !== '') {
            $model_content = $this->handleContent($model_content, $_name, $name);
            $this->createFile($model_filename, $model_content);
            $output->writeln('[SUCCESS]'.$_name.' 模型创建成功');
        } else {
            $output->writeln('[WARNING]'.$_name.' 模型创建失败');
        }
        
        return self::SUCCESS;
    }
    
    /**
     * @desc 模板文件内容处理
     * @date 2022-03-17
     * @param  string  $content
     * @param  string  $name
     * @param  string|null  $table_name
     * @return string
     */
    protected function handleContent(string $content, string $name, ?string $table_name = ''): string
    {
        // 时区设置
        $timezone = config('app.default_timezone') ?? 'Asia/Shanghai';
        date_default_timezone_set($timezone);
        
        // 数据处理
        $search = ['${FileName}', '${CreateTime}', '${TableName}'];
        $replace = [$name, date('Y-m-d h:i:s A', time()), $table_name];
        
        return str_replace($search, $replace, $content);
    }
    
    /**
     * 创建文件
     * @param  string  $filename
     * @param  string  $content
     * @return void
     */
    protected function createFile(string $filename, string $content)
    {
        if (!is_file($filename)) {
            file_put_contents($filename, $content);
        }
    }
    
}
