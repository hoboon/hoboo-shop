<?php

namespace app\controller;

use app\model\Test;
use support\Request;
use support\Response;
use Tinywan\Storage\Storage;

class Index
{
    public function index(Request $request)
    {
        $data = date('Y-m-d H:i:s', time());
        return response('['.$data.'] '.'Hoboo Intelligent Framework...111');
    }
    
    public function view(Request $request)
    {
        return view('index/view', ['name' => 'webman2222']);
    }
    
    /**
     * 接口测试
     * @param  Request  $request
     * @return Response
     */
    public function json(Request $request): Response
    {
        $data = [
            'keywords' => $request->get('keywords'),
            'type'     => 1,
        ];
        
        // $test_list = Test::getList($data);
        $test_list = [];
        $request_log = [
            'ip'           => $request->getLocalIp(),
            'port'         => $request->getLocalPort(),
            'real_ip'      => $request->getRealIp(),
            'remote_ip'    => $request->getRemoteIp(),
            'remote_port'  => $request->getRemotePort(),
            'url'  => $request->url(),
            'fullUrl'  => $request->fullUrl(),
            'method'  => $request->method(),
            'app_path'     => app_path(),
            'config_path'  => config_path(),
            'base_path'    => base_path(),
            'runtime_path' => runtime_path(),
        ];
        
        return json([
            'code' => 0,
            'msg'  => 'ok',
            'data' => array_merge($request_log, $test_list)
        ]);
    }
    
    /**
     * 生成指定数量的UUID
     *
     * @param  Request  $request
     * @return Response
     */
    public function getUuid(Request $request): Response
    {
        $result['data'] = [];
        
        if ((int) $count = $request->get('count')) {
            for ($i = 1; $i <= $count; $i++) {
                $result['data'][] = uniqid();
            }
        }
        
        return json($result);
    }
    
    /**
     * 文件上传测试测试
     *
     * @param  Request  $request
     * @return Response
     */
    public function uploadFile(Request $request): Response
    {
        /**
         * 指定存储方式：默认为本地存储
         * 默认处处位置：runtime 目录
         */
        $storage = 'local';
    
        /**
         * 初始化
         */
        Storage::config($storage);
        
        $result['uploadFile'] = Storage::uploadFile();
    
    
        /**
         * 上传响应信息
         */
        // {
        //     "uploadFile": [
        //         {
        //             "key": "filessss",
        //             "origin_name": "IMG_3605线稿.png",
        //             "save_name": "37f6c3f3d3250597f6d5bd828007b66dbda03994.png",
        //             "save_path": "/Users/mr.box/Downloads/Project/hb-webman-template/runtime/storage20220504/37f6c3f3d3250597f6d5bd828007b66dbda03994.png",
        //             "url": "http://127.0.0.1:8787/runtime20220504/37f6c3f3d3250597f6d5bd828007b66dbda03994.png",
        //             "unique_id": "37f6c3f3d3250597f6d5bd828007b66dbda03994",
        //             "size": 170002,
        //             "mime_type": "image/png",
        //             "extension": "png"
        //         }
        //     ]
        // }
        
        
        return json($result);
    }
    
}
