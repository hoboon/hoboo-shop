<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : Str
 * @Author          : mr.box
 * @Createtime      : 2022/4/12 15:51
 * @Description     : ...
 */

namespace hoboo\helper;

class Str
{
    // 缓存：下滑线转驼峰（首字母大写）
    protected static array $ucCamelizeCache = [];
    // 缓存：下滑线转驼峰（首字母小写）
    protected static array $lcCamelizeCache = [];
    
    /**
     * @desc 下滑线转驼峰（首字母大写）
     * @param  string  $value
     * @return string
     */
    public static function ucCamelize(string $value): string
    {
        $key = $value;
        if (isset(static::$ucCamelizeCache[$key])) {
            return static::$ucCamelizeCache[$key];
        }
        
        $value = ucwords(str_replace(['-', '_'], ' ', $value));
        
        return static::$ucCamelizeCache[$key] = str_replace(' ', '', $value);
    }
    
    /**
     * @desc 下滑线转驼峰（首字母小写）
     * @param  string  $value
     * @return string
     */
    public static function lcCamelize(string $value): string
    {
        if (isset(static::$lcCamelizeCache[$value])) {
            return static::$lcCamelizeCache[$value];
        }
        
        return static::$lcCamelizeCache[$value] = lcfirst(static::ucCamelize($value));
    }
}
