<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : HobooController
 * @Author          : mr.box
 * @Createtime      : 2022/3/3 11:50 AM
 * @Description     : ...
 */


namespace hoboo\controller;


use hoboo\service\HobooService;
use hoboo\traits\Help;

abstract class HobooController
{
    use Help;
    
    /**
     * 访问客户端 pc | mobile | h5 | wechat | .....
     *
     * @var string
     */
    protected string $app_client = 'pc';

    protected HobooService $service;
    
    /**
     * 获取请求提交数据
     *
     * @param $request
     * @return mixed
     */
    protected function getData($request): mixed
    {
        $data = $request->all();
        return is_array($data) ? $data : json_decode($request->rawBody(), true);
    }
}
