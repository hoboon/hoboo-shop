<?php
/**
 * @Project Name    : hb-crm
 * @File Name       : SmsServe
 * @Author          : mr.box
 * @Createtime      : 2022/6/30 12:19
 * @Description     : ...
 */


namespace hoboo\serve;


use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\SendSmsRequest;
use AlibabaCloud\Tea\Exception\TeaError;
use AlibabaCloud\Tea\Utils\Utils\RuntimeOptions;
use app\api\model\SmsAction;
use app\api\model\SmsConfig;
use app\api\model\SmsLog;
use Darabonba\OpenApi\Models\Config;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;

class SmsServe extends HobooController
{
    protected string $type;         // 短信平台类型
    protected string $type_uuid;    // 配置标识
    protected string $action_uuid;  // 模板标识
    protected string $action;       // 模板名称
    protected mixed $config;        // 短信配置
    protected mixed $sms_params;    // 短信内容
    
    /**
     * @param  array  $_config
     * @throws BadRequestHttpException
     */
    public function __construct(array $_config = [])
    {
        if (!empty($_config['type'])) {
            if (!in_array($_config['type'], ['system', 'aliyun'])) {
                throw new BadRequestHttpException('调用了暂不支持的短信服务商，当前支持：aliyun');
            }
            
            $this->type = $_config['type'];
        } else {
            $this->type = config('plugin.hoboo.sms.app.default');
        }
        
        /**
         * 获取库表短信配置记录
         */
        $this->getSmsConfig();
    }
    
    /**
     * 发送短信
     * @param $data
     * @return array|bool
     * @throws BadRequestHttpException
     */
    public function sendSms($data): bool|array
    {
        $data['uuid'] = $data['uuid'] ?? uniqid();
        
        $this->action = $data['action'] ?? 'account_login';
        $this->getSmsAction();
        
        return match ($this->type) {
            'aliyun' => $this->sendByAliyun($data),
            'system' => throw new BadRequestHttpException('还没开发'),
            default => throw new BadRequestHttpException('短信没发送'),
        };
    }
    
    /**
     * @desc 发送 阿里云短信
     * @param  array  $data
     * @return array|bool
     * @throws BadRequestHttpException
     */
    private function sendByAliyun(array $data): bool|array
    {
        // 基础配置
        $_config = new Config($this->config);
        
        // 创建服务
        $client = new Dysmsapi($_config);
        
        // 组装短信内容
        $params = array_merge($this->sms_params, [
            'phoneNumbers'  => $data['phone_number'],
            'templateParam' => "{'code': '".$data['code']."'}",
            'outId'         => $data['uuid']
        ]);
        
        $sendSmsRequest = new SendSmsRequest($params);
        
        $runtime = new RuntimeOptions([]);
        
        /**
         * 发送短信
         */
        try {
            $response = $client->sendSmsWithOptions($sendSmsRequest, $runtime);
        } catch (\Exception $error) {
            if (!($error instanceof TeaError)) {
                $error = new TeaError([], $error->getMessage(), $error->getCode(), $error);
                throw new BadRequestHttpException($error);
            }
        } finally {
            // 构建日志内容
            $log = [
                'uuid'         => $data['uuid'],
                'type_uuid'    => $this->type_uuid,
                'action_uuid'  => $this->action_uuid,
                'phone_number' => $data['phone_number'],
                'param'        => $params,
                'callback'     => $response ?? ($error ?? null),
                'remarks'      => '',
                'state'        => (int) isset($response) ?? 0
            ];
            
            // 记录调用日志
            (new SmsLog())->create($log);
        }
        
        return true;
    }
    
    /**
     * 获取短信配置
     * @return void
     * @throws BadRequestHttpException
     */
    private function getSmsConfig(): void
    {
        switch ($this->type) {
            case 'aliyun':
                // 短信配置
                $_config = (new SmsConfig())->where(['type' => $this->type])->first();
                
                // 结果校验
                if (is_null($_config)) {
                    throw new BadRequestHttpException('短信服务配置信息不存在');
                }
                
                // 配置标识
                $this->type_uuid = $_config['uuid'];
                
                // 短信签名
                $this->sms_params = [
                    'signName' => $_config['sign_name']
                ];
                
                $this->config = [
                    'accessKeyId'     => $_config['access_key'],
                    'accessKeySecret' => $_config['access_secret'],
                    '$_config'        => $_config
                ];
                break;
            case 'system':
                $_config = [
                    'sign_name'     => 'system_sign_name',
                    'access_key'    => 'system_access_key',
                    'access_secret' => 'system_access_secret',
                ];
                
                $this->config = [
                    'accessKeyId'     => $_config['access_key'],
                    'accessKeySecret' => $_config['access_secret'],
                ];
                break;
            default:
                $this->config = [];
        }
    }
    
    /**
     * 获取短信模板
     * @return void
     * @throws BadRequestHttpException
     */
    private function getSmsAction(): void
    {
        // 前置判断题哦阿健
        if (!isset($this->action) || !isset($this->type_uuid)) {
            throw new BadRequestHttpException('短信模板配置错误');
        }
        
        // 短信模板查询条件
        $where = [
            'type_uuid' => $this->type_uuid,
            'name'      => $this->action,
        ];
        
        // 查询短信模板
        $_action = (new SmsAction())->where($where)->first();
        
        // 结果校验
        if (is_null($_action)) {
            throw new BadRequestHttpException('短信服务配置信息不存在');
        }
        
        // 短信模板判断
        if (!isset($_action['template_id'])) {
            throw new BadRequestHttpException('短信模板编号不存在');
        }
        
        // 构建部分短信发送字段
        switch ($this->type) {
            case 'aliyun':
                $this->sms_params['templateCode'] = $_action['template_id'];
                break;
            case 'system':
                $this->sms_params['template_id'] = $_action['template_id'];
                break;
            default:
                $this->sms_params['template_id'] = null;
        }
        
        // 短信模板标识
        $this->action_uuid = $_action['uuid'];
    }
}
