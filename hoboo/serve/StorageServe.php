<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : Upload
 * @Author          : mr.box
 * @Createtime      : 2022/6/9 10:28
 * @Description     : ...
 */


namespace hoboo\serve;


use app\api\model\UploadLog;
use app\api\model\UploadType;
use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use Tinywan\Storage\Exception\StorageException;
use Tinywan\Storage\Storage;

class StorageServe extends HobooController
{
    protected string $storage_type;
    protected array $allow_type;    // 允许上传文件类型
    
    public function __construct()
    {
        $this->storage_type = 'local';
        // 获取允许上传的文件类型
        $this->getUploadType();
    }
    
    /**
     * @desc 上传文件
     * @param  array  $data
     * @return array
     * @throws BadRequestHttpException
     */
    public function uploadFile(array $data): array
    {
        /**
         * 初始化存储服务
         */
        Storage::config($data['storage'] ?? $this->storage_type);
        
        try {
            $result['data'] = Storage::uploadFile();
            foreach ($result['data'] as $key => $item) {
                /**
                 * 允许上传类型校验
                 * 非允许上传文件记录重置文件状态（文件已上传，仅为状态标记）
                 */
                if (!in_array($item['extension'] ?? '', $this->allow_type)) {
                    $result['data'][$key]['state'] = 0;
                    throw new BadRequestHttpException($item['extension'].' 类型文件无法上传');
                }
                
                // 仅当文件为服务器存储时，处理文件相对地址
                if($this->storage_type === 'local') {
                    $result['data'][$key]['path'] = substr($item['url'], strlen(env('APP_DOMAIN')) + 1);
                }
            }
        } catch (StorageException $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        } finally {
            // 日志上传
            foreach ($result['data'] as $item) {
                $this->setUploadLog($item);
            }
        }
        
        return $result;
    }
    
    /**
     * 记录上传日志
     * @param  array  $data
     * @return void
     */
    public function setUploadLog(array $data): void
    {
        $upload_log = [
            'uuid'          => $data['unique_id'],
            'operator_uuid' => $data['operator_uuid'] ?? 'system_upload',
            'storage_type'  => $this->storage_type,
            'file_type'     => $data['extension'],
            'origin_name'   => $data['origin_name'],
            'save_name'     => $data['save_name'],
            'save_path'     => $data['save_path'],
            'url'           => $data['url'],
            'size'          => $data['size'],
            'mime_type'     => $data['mime_type'],
            'extension'     => $data['extension'],
            'remarks'       => '',
            'state'         => $data['state'] ?? 1,
        ];
        
        UploadLog::setItem($upload_log);
    }
    
    /**
     * 获取允许上传的文件类型
     * @return void
     */
    private function getUploadType(): void
    {
        $this->allow_type = [];
        
        // 获取库表记录
        $items = (new UploadType())->where(['state' => 1])->get();
        
        // 库表数据获取判断
        if (!is_null($items)) {
            $this->allow_type = array_column($items->toArray(), 'type');
        }
    }
}
