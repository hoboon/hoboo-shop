<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : account
 * @Author          : mr.box
 * @Createtime      : 2022/3/8 9:35 PM
 * @Description     : ...
 */


namespace hoboo\serve;


use hoboo\controller\HobooController;
use hoboo\exception\BadRequestHttpException;
use support\Cache;
use support\Request;
use Workerman\Protocols\Http\Session;

class Account extends HobooController
{
    protected Session|bool $session;
    
    public function __construct(Request $request)
    {
        $this->session = $request->session();
    }
    
    /**
     * @desc 创建一个验证码键值
     * @param  array  $data
     * @return string
     */
    public function createLoginKey(array $data): string
    {
        $key = $data['phone_number'] ?? uniqid(microtime(true), true);
        
        $this->session->set('ac_captcha'.$key, $data['code']);
        
        return $key;
    }
    
    /**
     * @desc 校验一个验证码
     *
     * @param  array  $data
     * @return void
     * @throws BadRequestHttpException
     */
    public function checkCode(array $data): void
    {
        $key = $data['key'] ?? $data['phone_number'];
        
        $_code = $this->session->get('ac_captcha'.$key);
        
        // 验证码过期
        if (!$_code) {
            throw new BadRequestHttpException('验证码已过期');
        }
        
        // 验证码错误
        if (strtolower($_code) !== strtolower($data['code'])) {
            throw new BadRequestHttpException('验证码错误');
        }
        
        // 验证通过删除验证码
        $this->session->forget('ac_captcha'.$key);
    }
}
