<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : HobooModel
 * @Author          : mr.box
 * @Createtime      : 2022/4/12 01:38
 * @Description     : ...
 */


namespace hoboo\model;


use hoboo\helper\Str;
use hoboo\traits\ModelExtractDate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class HobooModel extends Model
{
    use SoftDeletes;
    use ModelExtractDate;
    
    /**
     * 与模型关联的数据表.
     * @var string
     */
    protected $table = '';
    
    /**
     * 与数据表关联的主键.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    
    /**
     * 指示模型是否主动维护时间戳。
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * 模型日期字段的存储格式。
     *
     * @var string
     */
    // protected $dateFormat = 'U';
    
    /**
     * 表中的创建时间，对应表中需要存在该字段
     */
    const CREATED_AT = 'create_time';
    
    /**
     * 表中的更新时间，对应表中需要存在该字段
     */
    const UPDATED_AT = 'update_time';
    
    /**
     * 表中的删除时间，需要软删除的时候使用，对应表中需要存在该字段
     */
    const DELETED_AT = 'delete_time';
    
    /**
     * 数组中的属性会被隐藏。
     *
     * @var array
     */
    protected $hidden = [
        'delete_time',
    ];
    
    /**
     * 类型转换
     *
     * @var array
     */
    protected $casts = [
        'create_time' => 'datetime:Y/m/d H:i:s',
        'update_time' => 'datetime:Y/m/d H:i:s',
    ];
    
    /**
     * 可批量赋值的属性。
     *
     * @var array
     */
    protected $fillable = [];
    
    /**
     * 不可以批量赋值的属性
     *
     * @var array
     */
    protected $guarded = [];
    
    /**
     * 默认关联作用域
     *
     * @var array
     */
    protected $search = [
        'state',
        'date',
        'keywords'
    ];
    
    /**
     * 指定查询库表字段
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeVisible(Builder $query, $data): Builder
    {
        if (!empty($data)) {
            $query->select($data);
        }
        
        return $query;
    }
    
    /**
     * 分页参数处理
     *
     * @param  array  $data
     * @return array
     */
    protected function handlePage(array $data): array
    {
        if (!isset($data['page']) && !isset($data['pageSize'])) {
            return [];
        }
        
        return [
            'total'    => $data['total'] ?? 0,
            'page'     => isset($data['page']) && (int) $data['page'] > 0 ? (int) $data['page'] : 1,
            'pageSize' => isset($data['pageSize']) && (int) $data['pageSize'] > 0 ? (int) $data['pageSize'] : 20,
        ];
    }
    
    /**
     * 分页数据处理
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopePage(Builder $query, $data): Builder
    {
        if (isset($data['pageSize'])) {
            $query->limit($data['pageSize']);
        }
        
        if (isset($data['page'])) {
            $query->offset(($data['page'] - 1) * $data['pageSize'] ?? 0);
        }
        
        return $query;
    }
    
    /**
     * 搜索数据处理
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeKeywords(Builder $query, $data): Builder
    {
        // 搜索条件
        if (isset($data['keywords'])) {
            // 搜索参数，默认搜索 'name'
            $keyword_by = !empty($data['keywordsBy']) ? $data['keywordsBy'] : 'name';
            $query->where($keyword_by, 'like', '%'.$data['keywords'].'%');
        }
        
        return $query;
    }
    
    /**
     * 最新数据
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeNewest(Builder $query, $data): Builder
    {
        // 搜索条件
        if (isset($data['newest']) && $data['newest']) {
            $query->orderBy('create_time', 'desc');
        } else {
            $query->orderBy('id');
        }
        
        return $query;
    }
    
    /**
     * 状态数据处理
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeState(Builder $query, $data): Builder
    {
        
        if (isset($data['state'])) {
            $query->where('state', $data['state']);
        }
        
        return $query;
    }
    
    /**
     * 时间数据处理
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeDate(Builder $query, $data): Builder
    {
        /**
         * 定义：
         * date_by  时间参数，默认参数 'create_time'
         * date     时间条件，object | array | string
         */
        $date_by = !empty($data['dateBy']) ? $data['dateBy'] : 'create_time';
        
        if (isset($data['date'])) {
            list($start_time, $end_time) = $this->getModelDate($data['date']);
            
            if ($start_time && $end_time) {
                $query->whereBetween($date_by, [$start_time, $end_time]);
            } elseif ($start_time && !$end_time) {
                $query->where($date_by, '>', $start_time);
            } elseif (!$start_time && $end_time) {
                $query->where($date_by, '<', $end_time);
            }
        }
        
        return $query;
    }
    
    /**
     * 根据 uuid 获取一条数据
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeUuid(Builder $query, $data): Builder
    {
        if (isset($data['uuid'])) {
            $query->where('uuid', '=', $data['uuid']);
        }
        
        return $query;
    }
    
    /**
     * 根据 id 获取一条数据
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeId(Builder $query, $data): Builder
    {
        if (isset($data['id'])) {
            $query->whereIn('id', is_array($data['id']) ? $data['id'] : [$data['id']]);
        }
        
        return $query;
    }
    
    /**
     * 根据 id 获取多条数据
     *
     * @param  Builder  $query
     * @param $data
     * @return Builder
     */
    protected function scopeIds(Builder $query, $data): Builder
    {
        if (isset($data['id'])) {
            return $query->whereIn('id', $data['id']);
        }
        
        return $query;
    }
    
    
    /**
     * 使用搜索器条件搜索字段
     *
     * @param  Builder  $query
     * @param $fields
     * @param  array  $data
     * @return Builder
     */
    public function scopeWithSearch(Builder $query, $fields, array $data = []): Builder
    {
        if (is_string($fields)) {
            $fields = explode(',', $fields);
        }
        
        foreach ($fields as $key => $field) {
            $fieldName = is_numeric($key) ? $field : $key;
            $method = Str::ucCamelize($fieldName);
            $scope = 'scope'.$method;
            
            if (method_exists($this, $scope)) {
                $query->$method($data);
            } elseif (isset($data[$field])) {
                $query->where($fieldName, $fieldName ? 'like' : '=',
                    $fieldName ? '%'.$data[$field].'%' : $data[$field]);
            }
        }
        
        return $query;
    }
    
}
