<?php

namespace hoboo\traits;

use support\Response;

trait Help
{
    /**
     * @param  $data
     * @param  array  $header
     * @return Response
     */
    protected function success($data, array $header = []): Response
    {
        $result = array_merge([
            'success' => true,
            'code'    => 'success',
        ], $data);
        
        return $this->hoboo_json($result, $header);
    }
    
    /**
     * @param  string|null  $code
     * @param  string|null  $message
     * @return Response
     */
    protected function fail(?string $message = 'SYSTEM_ERROR', ?string $code = 'error'): Response
    {
        $result = [
            'success' => false,
            'code'    => $code,
            'message' => $message,
        ];
        
        return json($result);
    }
    
    /**
     * @param $data
     * @param  array  $headers
     * @param  int  $options
     * @return Response
     */
    function hoboo_json($data, array $headers = [], int $options = JSON_UNESCAPED_UNICODE): Response
    {
        $headers = array_merge(['Content-Type' => 'application/json'], $headers);
        
        return new Response(200, $headers, json_encode($data, $options));
    }
}
