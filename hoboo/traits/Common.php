<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : Common
 * @Author          : mr.box
 * @Createtime      : 2022/5/13 16:06
 * @Description     : ...
 */


namespace hoboo\traits;


trait Common
{
    
    /**
     * 生成密码散列值
     *
     * @param $password
     * @return string
     */
    private function encodePassword($password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }
    
}
