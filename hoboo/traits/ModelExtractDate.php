<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : ${NAME}
 * @Author          : mr.box
 * @Createtime      : 2022/4/11 15:36
 * @Description     : ...
 */

namespace hoboo\traits;

trait ModelExtractDate
{
    protected string $date_format = 'Y-m-d H:i:s';
    
    /**
     * 处理日期时间范围
     *
     * @param $date
     * @return array
     */
    protected function getModelDate($date): array
    {
        if (is_string($date)) {
            $dateRange = $this->handleStringDate($date);
        } elseif (is_array($date)) {
            $dateRange = $this->handleArrayDate($date);
        } else {
            $dateRange = [];
        }
        
        return $dateRange;
    }
    
    /**
     * 单项时间处理
     *
     * @param  string  $date
     * @return array
     */
    private function handleStringDate(string $date): array
    {
        return match ($date) {
            'today' => [
                date($this->date_format, strtotime('today')),
                date($this->date_format, strtotime('tomorrow -1second'))
            ],
            'lately7' => [
                date('Y-m-d', strtotime("-7 day")),
                date($this->date_format)
            ],
            'lately30' => [
                date('Y-m-d', strtotime("-30 day")),
                date($this->date_format)
            ],
            'yesterday' => [
                date($this->date_format, strtotime('yesterday')),
                date($this->date_format, strtotime('today -1second'))
            ],
            'week' => [
                date($this->date_format, strtotime('this week 00:00:00')),
                date($this->date_format, strtotime('next week 00:00:00 -1second'))
            ],
            'month' => [
                date($this->date_format, strtotime('first Day of this month 00:00:00')),
                date($this->date_format, strtotime('first Day of next month 00:00:00 -1second'))
            ],
            'year' => [
                date($this->date_format, strtotime('this year 1/1')),
                date($this->date_format, strtotime('next year 1/1 -1second'))
            ],
            default => [],
        };
    }
    
    /**
     * 范围时间处理
     *
     * @param $date
     * @return array
     */
    private function handleArrayDate($date): array
    {
        $day_time = ' +1day -1second';
        
        if (!empty($date['start']) && empty($date['end'])) {
            $dateRange = [date($this->date_format, strtotime($date['start'])), ''];
        } elseif (empty($date['start']) && !empty($date['end'])) {
            $dateRange = ['', date($this->date_format, strtotime($date['end'].$day_time))];
        } elseif (!empty($date['start']) && !empty($date['end'])) {
            if ($date['start'] === $date['end']) {
                $dateRange = [
                    date($this->date_format, strtotime($date['start'])),
                    date($this->date_format, strtotime($date['start'].$day_time))
                ];
            } else {
                $dateRange = [
                    date($this->date_format, strtotime($date['start'])),
                    date($this->date_format, strtotime($date['end'].$day_time))
                ];
            }
        } else {
            $dateRange = [];
        }
        
        return $dateRange;
    }
}

