<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : Exception
 * @Author          : mr.box
 * @Createtime      : 2022/3/9 8:53 AM
 * @Description     : ...
 */


namespace hoboo\exception;


use Throwable;
use Webman\Exception\ExceptionHandler;
use Webman\Http\Request;
use Webman\Http\Response;

class ApiHandler extends ExceptionHandler
{
    public function render(Request $request, Throwable $exception): Response
    {
        
        return parent::render($request, $exception);
    }
}
