<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : UnauthorizedException
 * @Author          : mr.box
 * @Createtime      : 2022/5/3 16:12
 * @Description     : ...
 */


namespace hoboo\exception;


use Tinywan\ExceptionHandler\Exception\BaseException;

class UnauthorizedHttpException extends BaseException
{
    /**
     * HTTP 状态码
     *
     * @var int
     */
    public $statusCode = 401;
    
    /**
     * 错误消息
     *
     * @var string
     */
    public $errorMessage = '未经授权的请求';
    
    /**
     * 错误代码
     *
     * @var string
     */
    public $errorCode = 'error';
}
