<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : EmptyDataHttpException
 * @Author          : mr.box
 * @Createtime      : 2022/5/4 16:59
 * @Description     : ...
 */


namespace hoboo\exception;


use Tinywan\ExceptionHandler\Exception\BaseException;

class EmptyDataHttpException extends BaseException
{
    /**
     * HTTP 状态码
     *
     * @var int
     */
    public $statusCode = 404;
    
    /**
     * 错误消息
     *
     * @var string
     */
    public $errorMessage = '数据不存在';
    
    /**
     * 错误代码
     *
     * @var string
     */
    public $errorCode = 'error';
    
}
