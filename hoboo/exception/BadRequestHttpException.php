<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : BadRequestHttpException
 * @Author          : mr.box
 * @Createtime      : 2022/5/3 17:06
 * @Description     : ...
 */


namespace hoboo\exception;


use Tinywan\ExceptionHandler\Exception\BaseException;

class BadRequestHttpException extends BaseException
{
    /**
     * HTTP 状态码
     *
     * @var int
     */
    public $statusCode = 200;
    
    /**
     * 错误消息
     *
     * @var string
     */
    public $errorMessage = '请求错误';
    
    /**
     * 错误代码
     *
     * @var string
     */
    public $errorCode = 'error';
    
}
