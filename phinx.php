<?php

return
    [
        'paths'        => [
            'migrations' => 'database/migrations',
            'seeds'      => 'database/seeds'
        ],
        'environments' => [
            'default_migration_table' => 'phinxlog',
            "default_database"        => "dev",
            "default_environment"     => "dev",
            'dev'                     => [
                'adapter'      => 'pgsql',
                'host'         => '192.168.172.15',
                'port'         => '5432',
                'name'         => 'hbwebman',
                'user'         => 'hoboowebman',
                'pass'         => 'HNzhHBdNfL4L',
                'charset'      => 'utf8',
                'table_prefix' => 'hb_',
                'table_suffix' => '',
                'collation'    => 'utf8_unicode_ci',
            ],
        ]
    ];
