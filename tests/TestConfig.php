<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : TestConfig
 * @Author          : mr.box
 * @Createtime      : 2022/4/11 14:15
 * @Description     : ...
 */

use PHPUnit\Framework\TestCase;


class TestConfig extends TestCase
{
    public function testAppConfig()
    {
        $config = config('app');
        self::assertIsArray($config);   // 参数类型判断
        
        self::assertArrayHasKey('debug', $config);  // 参数存在性判断
        self::assertIsBool($config['debug']);   // 参数类型判断
        
        self::assertArrayHasKey('default_timezone', $config);  // 参数存在性判断
        self::assertIsString($config['default_timezone']);   // 参数类型判断
    }
}
