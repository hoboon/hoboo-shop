<?php
/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

return [
    // 默认数据库
    'default'     => env('DB_DEFAULT') ? : 'pgsql',
    // 各种数据库配置
    'connections' => [
        
        'mysql' => [
            'driver'      => 'mysql',
            'host'        => env('MYSQL_HOST') ? : '127.0.0.1',
            'port'        => env('MYSQL_PORT') ? : 3306,
            'database'    => env('MYSQL_DATABASE') ? : 'DATABASE',
            'username'    => env('MYSQL_USERNAME') ? : 'USERNAME',
            'password'    => env('MYSQL_PASSWORD') ? : 'PASSWORD',
            'unix_socket' => env('MYSQL_UNIX_SOCKET') ? : '',
            'charset'     => env('MYSQL_CHARSET') ? : 'utf8',
            'collation'   => env('MYSQL_COLLATION') ? : 'utf8_unicode_ci',
            'prefix'      => env('MYSQL_PREFIX') ? : '',
            'strict'      => env('MYSQL_STRICT') ? : true,
            'engine'      => env('MYSQL_ENGINE') ? : null,
        ],
        
        'sqlite' => [
            'driver'   => 'sqlite',
            'database' => env('SQLITE_DATABASE') ? : 'DATABASE',
            'prefix'   => env('SQLITE_PREFIX') ? : '',
        ],
        
        'pgsql' => [
            'driver'   => 'pgsql',
            'host'     => env('PGSQL_HOST') ? : '127.0.0.1',
            'port'     => env('PGSQL_PORT') ? : 5432,
            'database' => env('PGSQL_DATABASE') ? : 'DATABASE',
            'username' => env('PGSQL_USERNAME') ? : 'USERNAME',
            'password' => env('PGSQL_PASSWORD') ? : 'PASSWORD',
            'charset'  => env('PGSQL_CHARSET') ? : 'utf8',
            'prefix'   => env('PGSQL_PREFIX') ? : '',
            'schema'   => env('PGSQL_SCHEMA') ? : '',
            'sslmode'  => env('PGSQL_SSLMODE') ? : '',
        ],
        
        'sqlsrv' => [
            'driver'   => 'sqlsrv',
            'host'     => env('SQLSRV_HOST') ? : '127.0.0.1',
            'port'     => env('SQLSRV_PORT') ? : 1433,
            'database' => env('SQLSRV_DATABASE') ? : 'DATABASE',
            'username' => env('SQLSRV_USERNAME') ? : 'USERNAME',
            'password' => env('SQLSRV_PASSWORD') ? : 'PASSWORD',
            'charset'  => env('SQLSRV_CHARSET') ? : 'utf8',
            'prefix'   => env('SQLSRV_PREFIX') ? : '',
        ],
    ],
];
