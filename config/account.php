<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : ${NAME}
 * @Author          : mr.box
 * @Createtime      : 2022/3/8 9:14 PM
 * @Description     : ...
 */

return [
    'captcha_exp' => env('CAPTCHA_EXP') ? : 7200, // 登陆验证码有效期
];
