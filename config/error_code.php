<?php
/**
 * @Project Name    : hb-shop
 * @File Name       : ${NAME}
 * @Author          : mr.box
 * @Createtime      : 2022/3/3 2:29 PM
 * @Description     : ...
 */

use support\ErrorCode;

return [
    'class'            => new ErrorCode(),  // ErrorCode 类文件
    'root_path'        => app_path(),       // 当前代码根目录
    'system_number'    => 201,              // 系统标识
    'start_min_number' => 10000,            // 错误码生成范围
];
