<?php
/**
 * @Project Name    : hb-crm
 * @File Name       : sms server config
 * @Author          : mr.box
 * @Createtime      : 2022/6/30 13:31
 * @Description     : ...
 */

return [
    'enable'  => env('SMS_ENABLE') ? : false,
    'default' => 'aliyun',
];
